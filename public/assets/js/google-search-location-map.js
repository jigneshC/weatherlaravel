var geocoder;
var map;
var marker;
function initAutocomplete() {
    var input = document.getElementById('location_name');
    var searchBox = new google.maps.places.SearchBox(input);

    

    var defaultLat = document.getElementById('location_latitude').value;
    var defaultLon = document.getElementById('location_longitude').value;

    if (!defaultLat || defaultLat == null || defaultLat == 0) {
        defaultLat = document.getElementById('default_latitude').value;
    }
    if (!defaultLon || defaultLon == null || defaultLon == 0) {
        defaultLon = document.getElementById('default_longitude').value;
    }


    geocoder = new google.maps.Geocoder();

    var latlng = new google.maps.LatLng(defaultLat, defaultLon);

    var typeId = google.maps.MapTypeId.ROADMAP;

    //       var typeId = google.maps.MapTypeId.SATELLITE;

    var mapOptions = {
        zoom: 10,
        center: latlng,
        mapTypeId: typeId,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_LEFT
        },
        //mapTypeId: google.maps.MapTypeId.SATELLITE
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: latlng
    });

    google.maps.event.addListener(marker, 'dragend', function (event) {

        latitude = event.latLng.lat();
        longitude = event.latLng.lng();

        jQuery('#location_latitude').val(latitude);
        jQuery('#location_longitude').val(longitude);

        var draglatlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
        geocoder.geocode({'latLng': draglatlng}, function (data, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var add = data[1].formatted_address; //this is the full address
                jQuery('#location_name').val(add);
            } else {
                alert('Geocode was not successful for finding address for following reason: ' + status);
            }
        });
    });

    searchBox.addListener('places_changed', function(event) {
        var places = searchBox.getPlaces();
        longitude = places[0].geometry.location.lng();
        latitude = places[0].geometry.location.lat();

        map.setCenter(places[0].geometry.location);
        marker.setPosition(places[0].geometry.location);

        $(".location_longitude").val(longitude);
        $(".location_latitude").val(latitude);
    });
}
function mapRefresh() {
    var latitude = document.getElementById('location_latitude').value;
    var longitude = document.getElementById('location_longitude').value;
    var location_name = document.getElementById('location_name').value;
    
    if(latitude && latitude!=""){
        var draglatlng = new google.maps.LatLng(latitude,longitude);
        geocoder.geocode({'latLng': draglatlng}, function (data, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                marker.setPosition(draglatlng);
                map.setCenter(draglatlng);
            
                var add = data[1].formatted_address; //this is the full address
                jQuery('#location_name').val(add);
            } else {
                alert('Geocode was not successful for finding address for following reason: ' + status);
            }
        });
    }else if(location_name != ""){
         var address = document.getElementById('location_name').value;
         geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();

                jQuery('#location_latitude').val(latitude);
                jQuery('#location_longitude').val(longitude);

                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                
            } else {
                alert('Geocode was not successful for the following reason: ' + status);

            }
        });
    }
}
