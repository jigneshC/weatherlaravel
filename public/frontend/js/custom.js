(function($) {

	"use strict"

	$('#back-to-top').on("click", function() {
		// When arrow is clicked
		$('body,html').animate({
			scrollTop : 0 // Scroll to top of body
		},800);
		return false;
	});
	//$('.navbar-nav li a').on("click", function() {
		// When arrow is clicked
		//$('body,html').animate({
		//	scrollTop : 0 // Scroll to top of body
		//},800);
		//return false;
	//});
	
	//$('.down-arrow').on("click", function() {
//		// When arrow is clicked
//		$('body,html').animate({
//			offset:-62 // Scroll to top of body
//		},800);
//		return false;
//	});
	
	//$(".down-arrow").localScroll({
//		target:"body",duration:1500,offset:-62,easing:"easeInOutExpo";
//	});

	
	
})(jQuery);


function ajaxError(xhr, status, error) {
    if(xhr.status ==401){
        return "You are not logged in. please login and try again";
    }else if(xhr.status == 403){
        return "You have not permission for perform this operations";
    }else if(xhr.responseJSON && xhr.responseJSON.message!=""){
        return xhr.responseJSON.message;
    }else{
        return"Something went wrong , Please try again later.";
    }
}