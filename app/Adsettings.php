<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adsettings extends Model
{
    protected $table = 'adsettings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'ad_id','status','min_day','max_day','min_amount','max_amount','show_or_not','weather_type','qualifier','amount','frequency','hoursofday_from','hoursofday_to','day_of_week','enable_frequency','enable_hoursofday','enable_day_of_week','setting_end_time'
    ];
    
    public function adsets() {
        return $this->belongsTo('App\AdsetDB', 'adset_id', 'adset_id');
    }
    public function compaigns() {
        return $this->belongsTo('App\CampaignDB', 'campaign_id', 'campaign_id');
    }
    
    public  function getNextCheckTime(){
        $item =$this;
        $frequency_check_time = 1;
        $nexttime = null;
        
        if($item->enable_hoursofday && $item->enable_hoursofday ==1 && $item->hoursofday_from){
            $nexttime = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->format("Y-m-d")." ".$item->hoursofday_from);
             if($item->enable_frequency && $item->frequency ==3 ){
               
                $check = \Carbon\Carbon::now()->addMinute($frequency_check_time);
                
                if($check->format("H:i:s") >= $item->hoursofday_from && $check->format("H:i:s") <= $item->hoursofday_to){
                    $nexttime = \Carbon\Carbon::now()->addMinute($frequency_check_time);
                }
            }
            
            if($item->enable_day_of_week && $item->day_of_week){
                 $availabledays = json_decode($item->day_of_week,true);
                 if (!in_array($nexttime->dayOfWeek, $availabledays)){
                     $nexttime = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->format("Y-m-d")." ".$item->hoursofday_from);
                     $nexttime =  $item->getNextActiveDay($nexttime,$availabledays);
                 }
            }
            
        }
        if(!$nexttime){
            $nexttime = \Carbon\Carbon::now()->startOfDay();
            
            if($item->enable_frequency && $item->frequency ==3 ){
                $nexttime = \Carbon\Carbon::now()->addMinute($frequency_check_time);
            }
            
            if($item->enable_day_of_week && $item->day_of_week){
                 $availabledays = json_decode($item->day_of_week,true);
                 if (!in_array($nexttime->dayOfWeek, $availabledays)){
                     $nexttime = \Carbon\Carbon::now()->startOfDay();
                     $nexttime = $item->getNextActiveDay($nexttime,$availabledays);
                 }
            }
        }
        if(!$nexttime){
            $nexttime = \Carbon\Carbon::now()->startOfDay();
            
            if($item->enable_day_of_week && $item->day_of_week){
                 $availabledays = json_decode($item->day_of_week,true);
                 if (!in_array($nexttime->dayOfWeek, $availabledays)){
                     $nexttime =$item->getNextActiveDay($nexttime,$availabledays);
                 }
            }
        }
        if($nexttime && $nexttime <=\Carbon\Carbon::now()){
            $nexttime = $nexttime->addDay(1);
        }
        return $nexttime;
        
    }
    
    public function getNextActiveDay($nexttime,$availabledays){
        $result = null;
        
        for($i=0; $i<=6 ; $i++){
            if (!in_array($nexttime->addDay(1)->dayOfWeek, $availabledays)){
                $result = $nexttime;
                break;
            }
        }
        return $result;
    }
}
