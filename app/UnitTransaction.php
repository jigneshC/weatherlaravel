<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\WebSite;

class UnitTransaction extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_transaction';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function billing()
    {
        return $this->belongsTo('App\Billing', 'billing_id', 'id');
    }
    
    public static function creditUnits($website_id, $unit, $reference) {
        $result = [];

        $requestData = [
            'website_id' => $website_id,
            'unit' => $unit,
            'reference' => $reference,
            'unit_type' => "credit"
        ];

        $ob = null;
        $website = Website::where('id', $website_id)->first();

        if ($website) {
            $ob = UnitTransaction::create($requestData);
        }

        if ($ob) {
            $website->units = $website->units + $unit;
            $website->save();
            $result['message'] = \Lang::get('moduleunit.responce_msg.unit_credited_to_website_success');
            $result['code'] = 200;
        } else {

            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }


        return $result;
    }

}
