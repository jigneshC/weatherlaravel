<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\AdaccountDB;
use App\CampaignDB;
use App\AdsetDB;
use App\FbadDB;

use Illuminate\Support\Facades\Auth;
use Session;


class FbuserDB extends Model
{
    protected $table = 'fbusers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name', 'user_id', 'fb_id',
    ];
    
    public static function tetestic()
    {
        $fb_id = Session::get('fb_id');
        $auth_user_id = Auth::user()->id;
        
        $adaccounts = AdaccountDB::whereIn('account_id',Session::get('account_ids'))->get();
        $adcompigns = CampaignDB::whereIn('account_id',Session::get('account_ids'))->get();
        $adsets = AdsetDB::whereIn('account_id',Session::get('account_ids'))->get();
        $ads = FbadDB::whereIn('account_id',Session::get('account_ids'))->get();
        
        return ['adaccounts'=>$adaccounts,'adcompigns'=>$adcompigns,'adsets'=>$adsets,'ads'=>$ads];
    }
}
