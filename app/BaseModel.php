<?php

namespace App;

use App\Scope\GlobalWebSiteScope;
use Illuminate\Database\Eloquent\Model;


use App\Traits\BaseModelTrait;

class BaseModel extends Model
{
    use BaseModelTrait;
}
