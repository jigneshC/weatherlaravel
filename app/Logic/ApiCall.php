<?php

namespace App\Logic;

use Auth;
use App\Temprature;


class ApiCall
{



    public static function getWeatherByLtLong($lat,$long)
    {
        $last_api_use = 30;
        $temp = Temprature::where("location_latitude",$lat)->where("location_longitude",$long)
                            ->where("created_at",">=",\Carbon\Carbon::now()->subMinute($last_api_use))->first();
        if($temp){
            return $temp;
        }
        
        $res = null;
        $WEATHER_API_KEY = config('settings.weather_key.WEATHER_API_KEY');
        $URL = config('settings.weather_key.WEATHER_API_URL');
        try{
            $uri = "units=metric&lat=".$lat."&lon=".$long."&apikey=".$WEATHER_API_KEY;
           //['units'=>'metric','lat'=>$lat,'lon'=>$long,'apikey'=>$WEATHER_API_KEY];
            $url =  $URL."?".$uri;
            $ch = curl_init ($url); // your URL to send array data
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec ($ch);
            
            $result = json_decode($result,true);
           // echo "<pre>";            print_r($result); exit;
            if(isset($result['cod']) && $result['cod']==200){
                $res = ApiCall::storeWeatherInfoByLatLong($lat, $long, $result);
            }
            
        

        }catch (\Error $E){
            $E->getMessage();
        }
        return $res;
    }
    public static function storeWeatherInfoByLatLong($lat,$long,$result)
    {
        $temprature = null;
        foreach ($result['list'] as $k=>$list) {
            $date = \Carbon::createFromTimestamp($list['dt']);
            $temprature = Temprature::where("location_longitude",$long)->where("location_latitude",$lat)->where("weather_date",$date)->first();
            if(!$temprature){
                $temprature = new Temprature();
                $temprature->weather_date = $date;
                $temprature->temp = $list['main']['temp_max'];
            }
            
            
                
            $temprature->location_longitude = $long;
            $temprature->location_latitude = $lat;
            
            /*if($temprature->min_temp > $list['main']['temp_min']){
                $temprature->min_temp = $list['main']['temp_min'];
            }*/
            if($temprature->temp < $list['main']['temp_max']){
                $temprature->temp = $list['main']['temp_max'];
            }
            $temprature->pressure = $list['main']['pressure'];
            $temprature->humidity = $list['main']['humidity'];
            $temprature->cloud = $list['clouds']['all'];
            $temprature->wind = ($list['wind']['speed'])? $list['wind']['speed']* 3.6 : 0; // meter/second to km/hr
             
            
            $temprature->save();
            
           
        }
        return $temprature;
        
    }
    

}