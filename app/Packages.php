<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'packages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','slug_id', 'desc','price','currency','interval_count','interval','package_class','image','details','status','created_by','updated_by'];

    
}
