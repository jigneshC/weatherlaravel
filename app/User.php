<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\HasRoles;

use Auth;
use Session;
use App\CampaignDB;
class User extends Authenticatable {

    use Notifiable,
        HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'activation_token', 'activation_time', 'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function setUserSession() {
        $auth_user_id = Auth::user()->id;
        $adaccountids = [];
        if (!Session::has('fb_access_token')) {
            $adaccount = \App\AdaccountDB::where('user_id', $auth_user_id)->where('tokenlonglive_expires_time', ">", \Carbon\Carbon::now())->orderby("updated_at", "DESC")->first();
            if ($adaccount && $adaccount->accesstokenLongLive && $adaccount->accesstokenLongLive != "") {
                Session::put('fb_access_token', $adaccount->accesstokenLongLive);
                Session::put('fb_id', $adaccount->fb_id);

                $adaccountids = \App\AdaccountDB::where('user_id', $auth_user_id)->where('tokenlonglive_expires_time', ">", \Carbon\Carbon::now())->orderby("updated_at", "DESC")->pluck("account_id")->toArray();
                if ($adaccountids && isset($adaccountids)) {
                    Session::put('account_ids', $adaccountids);
                } else {
                    Session::put('account_ids', []);
                }
            } else {
                Session::put('account_ids', []);
            }
        } else {
            Session::put('account_ids', []);
        }
        
        $adcompigns = CampaignDB::whereIn('account_id',$adaccountids)->get();
        Session::put('adcompigns_count',$adcompigns->count());
    }

}
