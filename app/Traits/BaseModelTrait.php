<?php
namespace App\Traits;


use App\UnitTransaction;
use App\UnitsModule;
use App\WebSite;

use Auth;
use Session;
trait BaseModelTrait
{

    public static function boot()
    {
        parent::boot();

        
        static::created(function ($model) {
            BaseModelTrait::resetSession($model->getTable(),"create");
        });
        static::updated(function ($model) {
            BaseModelTrait::resetSession($model->getTable(),"edit");
        });
        static::deleted(function ($model) {
            BaseModelTrait::resetSession($model->getTable(),"delete");
        });



        
    }

    public static function resetSession($table,$method)
    {
        Session::forget('session_notification_header1');
        Session::forget('session_notification_header2');
        Session::forget('session_notification_set_time');
        Session::forget('adcompigns_count');
    }


    

}