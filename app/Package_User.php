<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package_User extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'package_user';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['package_id', 'user_id','status'];

}
