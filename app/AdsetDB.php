<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsetDB extends Model
{
    protected $table = 'adsets';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'id','adset_id', 'account_id','fb_id','campaign_id','name','status','created_time','geo_locations','max_temp','min_temp','location_longitude','location_latitude','location_name','location_city','location_postalcode','location_regions','location_country','location_countrycode'
    ];
    
    public function ads() {
        return $this->hasMany('App\FbadDB', 'adset_id', 'adset_id');
    }
    
    public function compaigns() {
        return $this->belongsTo('App\CampaignDB', 'campaign_id', 'campaign_id');
    }
   
}
