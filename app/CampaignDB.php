<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignDB extends Model {

    protected $table = 'campaigns';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'account_id','fb_id', 'campaign_id', 'name', 'status', 'created_time',
    ];

    public function adsets() {
        return $this->hasMany('App\AdsetDB', 'campaign_id', 'campaign_id');
    }

    public function ads() {
        return $this->hasMany('App\FbadDB', 'campaign_id', 'campaign_id');
    }
}
