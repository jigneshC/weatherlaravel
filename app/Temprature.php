<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temprature extends Model
{
    protected $table = 'temprature';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'location_city', 'location_postalcode', 'location_regions','location_country','location_countrycode','location_longitude','location_latitude','temp','humidity','pressure','uv','rain','snow','sunny','cloud','wind','weather_date'
    ];
    
    
}
