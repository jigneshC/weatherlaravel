<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
class Billing extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'billing';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function package()
    {
        return $this->belongsTo('App\Packages', 'package_id', 'id');
    }
    
    public function transaction()
    {
        return $this->hasMany('App\UnitTransaction', 'billing_id', 'id');
    }
    
}
