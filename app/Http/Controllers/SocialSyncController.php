<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FbuserDB;
use App\AdaccountDB;
use App\CampaignDB;
use App\AdsetDB;
use App\FbadDB;
use Auth;
use Session;
use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccountUser;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdAccountUserFields;

class SocialSyncController extends Controller {

    public function __construct() {

        /* view()->composer('*', function ($view) {
          $view->with('tetestic',FbuserDB::tetestic());
          }); */
        // View::share('tetestic',  \App\FbuserDB::tetestic());
    }

    public function dashboard() {
        return view('user-backend.index');
    }

    public function callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {

        // Obtain an access token.
        $token = "";
        $error = "";
        try {
            $token = $fb->getAccessTokenFromRedirect();
            $expire_in = 7200;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $error = $e->getMessage();
        }

        if (!$token) {
            $error = "No Token found";
        }


        if ($error == "") {
            $oauth_client = $fb->getOAuth2Client();
          //  echo "<pre>";            print_r($oauth_client); exit;
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
                 $expire_in = 5181395;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                $error = $e->getMessage();
            }
        }

        $fb->setDefaultAccessToken($token);

        try {
            $response = $fb->get('/me?fields=id,name,email');
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $error = $e->getMessage();
        }

        if ($error == "") {
            $facebook_user = $response->getGraphUser();
        }
        if ($error == "" && $facebook_user) {
            $tokena = (string) $token;
            $result = $this->syncFacebookData($tokena,$facebook_user,$expire_in);
            if($result['code'] == 400){
                $error = $result['message'];
            }else{
                return redirect('Billing/subscription');
            }
        }
        
        if ($error != "") {
            Session::flash('flash_error', $error);
        }
        return redirect('social-sync');
    }

    public function syncFacebookData($accesstokenLongLive, $facebook_user, $expire_in) {

         $fb_id = $facebook_user['id'];
         $fb_username = $facebook_user['name'];
         $auth_user_id = Auth::user()->id;
        $tokenlonglive_expires_time = \Carbon\Carbon::now()->addSeconds($expire_in);


        $fb_user = FbuserDB::where('fb_id', $fb_id)->where('user_id', $auth_user_id)->first();
        if (!$fb_user) {
            $data['name'] = $fb_username;
            $data['fb_id'] = $fb_id;
            $data['user_id'] = $auth_user_id;
            $fbuser = FbuserDB::create($data);
        }
        try {


            Api::init(config('syncaccount.fb.FACEBOOK_APP_ID'), config('syncaccount.fb.FACEBOOK_APP_SECRET'), $accesstokenLongLive);
            $user = new AdAccountUser($fb_id);
            $user->read(array(AdAccountUserFields::ID));

            $accounts = $user->getAdAccounts(array(
                AdAccountUserFields::ID,
                AdAccountUserFields::NAME,
            ));

            foreach ($accounts as $account) {
                $adata['account_id'] = $account->id;
                $adata['name'] = $account->name;
                $adata['fb_id'] = $fb_id;
                $adata['user_id'] = $auth_user_id;
                $adata['accesstokenLongLive'] = $accesstokenLongLive;
                // $adata['accesstoken'] = $accesstoken;
                $adata['tokenlonglive_expires_time'] = $tokenlonglive_expires_time;
                //  $adata['token_expires_time'] = $token_expires_time;

                $adaccounts = AdaccountDB::where('account_id', $account->id)->where('user_id', $auth_user_id)->first();
                if ($adaccounts) {
                    $adaccounts->update($adata);
                } else {
                    $adaccount = AdaccountDB::create($adata);
                }
                $account = new AdAccount($account->id);

                $Campaigns = $account->getCampaigns($fields = array('id', 'name', 'account_id', 'status', 'created_time'));
                $adsets = $account->getAdSets($fields = array('id', 'name', 'account_id', 'campaign_id', 'created_time', 'status', 'targeting'));
                $ads = $account->getAds($fields = array('id', 'status', 'name', 'account_id', 'adset_id', 'campaign_id', 'configured_status', 'effective_status', 'created_time'));

                foreach ($Campaigns as $item) {
                    $obi = [
                        'account_id' => $account->id,
                        'campaign_id' => $item->id,
                        'name' => $item->name,
                        'status' => $item->status,
                        'created_time' => $item->created_time,
                        'fb_id' => $fb_id,
                    ];
                    $campaignexist = CampaignDB::where('campaign_id', $item->id)->first();
                    if ($campaignexist) {
                        $campaignexist->update($obi);
                    } else {
                        $campaignexist = CampaignDB::create($obi);
                    }
                }
                foreach ($adsets as $item) {
                    $geo_locations = [];
                    if (isset($item->targeting) && isset($item->targeting['geo_locations'])) {
                        $geo_locations = $item->targeting['geo_locations'];
                    }
                    $obi = [
                        'account_id' => $account->id,
                        'adset_id' => $item->id,
                        'campaign_id' => $item->campaign_id,
                        'name' => $item->name,
                        'status' => $item->status,
                        'created_time' => $item->created_time,
                        'fb_id' => $fb_id,
                        'geo_locations' => json_encode($geo_locations)
                    ];
                    $adsetexist = AdsetDB::where('adset_id', $item->id)->first();
                    if ($adsetexist) {
                        $adsetexist->update($obi);
                    } else {
                        $adsetexist = AdsetDB::create($obi);
                    }
                }
                foreach ($ads as $item) {
                    $obi = [
                        'ad_id' => $item->id,
                        'account_id' => $account->id,
                        'adset_id' => $item->adset_id,
                        'campaign_id' => $item->campaign_id,
                        'name' => $item->name,
                        'status' => $item->status,
                        'created_time' => $item->created_time,
                        'fb_id' => $fb_id
                    ];
                    $adexist = FbadDB::where('ad_id', $item->id)->first();
                    if ($adexist) {
                        $adexist->update($obi);
                    } else {
                        $adexist = FbadDB::create($obi);
                    }
                }
            }

            \App\User::setUserSession();

            $result = [];
            $result['message'] = "Success";
            $result['code'] = 200;
            Session::flash('flash_success', "Facebook Account Sync successfully !!");
        } catch (\Exception $e) {
           // echo "<pre>";            print_r($e); exit;
            if ($e->getCode() == 3) {
                $result['message'] = /* "Tht your Facebook Adwordps Maanger Account is yet not active. Please activate it and try again. "; */$e->getMessage(); //"Your Facebook Account Have No Any Business Ad Account !!";
                $result['code'] = 400;
            } else {
                $result['message'] = $e->getMessage();
                $result['code'] = 400;
            }
            Session::forget('fb_access_token');
            Session::forget('fb_id');
        }
        return $result;
    }

}
