<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\User;
use App\Setting;
use App\Billing;
use App\UnitTransaction;

use Session;
use Mail;
use Lang;
use View;

class EmailController extends Controller {





    
    public function sendMailOnBillingStart($billing_id) {
        
        $admin_to_mail = env("MAIL_ADMIN_TO_ADDRESS","jignesh.citrusbug@gmail.com");
        $admin_cc_email = [];
        $transaction = null;

        $setting = Setting::where('key','emails_as_cc')->first();
        if($setting && $setting->value != ''){
            $admin_cc_email = explode(',',$setting->value);
        }
        $admin_cc_email[] = $admin_to_mail;
       

        $billing = Billing::where("id",$billing_id)->first();


        if($billing && $billing->subscription_id && $billing->subscription_id!="" ){
            $package = json_decode($billing->package);
           
            $buyer_email =$billing->buyer_email;
            $user = $billing->user;
            
            if($user){
                $buyer_email = $user->email;
            }
           
            $subject = Lang::get('billing.mail_subject.user_create_subscription');
            $subject_admin = Lang::get('billing.mail_subject.admin_create_subscription');
                        
            $lang = "en";
            Mail::send('emails.billing.buyer.create-subscription', compact('billing', 'user','lang','package','transaction'), function ($message) use ($buyer_email, $subject) {
                $message->to($buyer_email)->subject($subject);
            });
           
            Mail::send('emails.billing.admin.create-subscription', compact('billing', 'user','lang','package','transaction'), function ($message) use ($subject_admin, $admin_to_mail,$admin_cc_email) {
                $message->to($admin_cc_email)->subject($subject_admin);
            });

        }
    }
    public function sendMailOnInvoiceGenerate($transaction_id) {
        
        $admin_to_mail = env("MAIL_ADMIN_TO_ADDRESS","jignesh.citrusbug@gmail.com");
        $admin_cc_email = [];

        $setting = Setting::where('key','emails_as_cc')->first();
        if($setting && $setting->value != ''){
            $admin_cc_email = explode(',',$setting->value);
        }
        $admin_cc_email[] = $admin_to_mail;
       

        $transaction = UnitTransaction::where("id",$transaction_id)->first();
        
        if($transaction && $transaction->billing){
            $billing = $transaction->billing;
            $package = json_decode($billing->package);
           
            $buyer_email =$billing->buyer_email;
            $user = $billing->user;
            
            if($user){
                $buyer_email = $user->email;
            }
           
            $subject = Lang::get('billing.mail_subject.user_invoice_subscription');
            $subject_admin = Lang::get('billing.mail_subject.admin_invoice_subscription');
                        
            $lang = "en";
            Mail::send('emails.billing.buyer.create-subscription-invoice', compact('billing', 'user','lang','package','transaction'), function ($message) use ($buyer_email, $subject) {
                $message->to($buyer_email)->subject($subject);
            });
           
            Mail::send('emails.billing.admin.create-subscription-invoice', compact('billing', 'user','lang','package','transaction'), function ($message) use ($subject_admin, $admin_to_mail,$admin_cc_email) {
                $message->to($admin_cc_email)->subject($subject_admin);
            });

        }
    }

    
}
