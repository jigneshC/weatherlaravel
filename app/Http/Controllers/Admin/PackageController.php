<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Packages;
use Session;
use Auth;
use Carbon;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class PackageController extends Controller
{
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.packages.index');
    }
    public function datatable(Request $request) {
        $record = Packages::where("id",">",0);
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();
        $requestData = $request->all();
        
        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'price' => 'required',
            'currency' => 'required',
            'interval' => 'required',
          //  'interval_count' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'details' => 'required',
            'details.*' => 'required',
            'status' => 'required',
        ]);

        $id = Auth::user()->id;
        $requestData['created_by'] = $id;
        $requestData['interval_count'] = 1;
        $requestData['details'] = json_encode($requestData['details']);
        $requestData['slug_id'] = $this->findUniqueSlug($requestData['name'], $requestData,'packages',0);
        if ($request->file('image')) {
            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('package'), $filename);
            $requestData['image'] = url('package').'/'.$filename;
        }             
        $module = Packages::create($requestData);
        if($module){
            $result['message'] = \Lang::get('comman.responce_msg.item_created_success',['item'=>"Unit Package"]);
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);

    }
    
    
    public function edit($id)
    {
        $result = array();
        $item = Packages::findOrFail($id);
        
        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);

    }

    public function update($id, Request $request)
    {
        $result = array();
        $requestData = $request->all();
        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'price' => 'required',
            'currency' => 'required',
            'interval' => 'required',
          //  'interval_count' => 'required',
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'details' => 'required',
            'details.*' => 'required',
            'status' => 'required',
        ]);

        $requestData['slug_id'] = $this->findUniqueSlug($requestData['name'], $requestData,'packages',$id);
        $item = Packages::where("id",$id)->first();
        $id = Auth::user()->id;
        $requestData['details'] = json_encode($requestData['details']);
        $requestData['updated_by'] = $id;

        if ($request->file('image')) {
            if($item->image != '')
            {
                $urlArray = explode('/', $item->image);
                $imageName = end($urlArray);
                if(file_exists(public_path('package').'/'.$imageName) AND !empty($imageName)){
                    unlink(public_path('package').'/'.$imageName);
                }
            }
            $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('package'), $filename);
            $requestData['image'] = url('package').'/'.$filename;            
        }
        if($item){
            $item->update($requestData);
            $result['message'] = \Lang::get('comman.responce_msg.record_updated_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/packages');
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Packages::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/packages');
        }
    }
    
    public function findUniqueSlug($name,$requestData,$table,$id=0)
    {
        $slug = str_slug($name);
        if($table=="packages"){
            $count = Packages::whereSlugId($slug)->where('id','!=',$id)->count();
            if($count >1){
                $slug = $slug."-".($count+1);
            }
            
        }
        return $slug;
    }
    public function generateStripePlan($id,Request $request)
    {
        $item = Packages::where("id",$id)->first();
        if(!$item){
            Session::flash('flash_error',"No Package Found");
            return redirect('admin/packages');
        }
        
        if($item->stripe_product_id && $item->stripe_product_id !=""){
            Session::flash('flash_error',"Strip Package Already Exist");
            return redirect('admin/packages');
        }
        
        $stripe = Stripe::make(env("STRIPE_SECRET"));
        $plan = $stripe->plans()->create([
            'id' => $item->slug_id,
            'name' => $item->name,
            'amount' => $item->price,
            'currency' => $item->currency,
            'interval' => $item->interval,
            'interval_count'=>$item->interval_count,
            'statement_descriptor' => $item->price." per ".$item->interval,
        ]);

        $item->stripe_product_id = $plan['product'];
        $item->save();
        
        Session::flash('flash_success',"Strip Package Created !!");
        return redirect('admin/packages');
    }


}
