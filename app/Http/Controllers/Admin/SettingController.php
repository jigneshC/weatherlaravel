<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use App\Setting;
use Session;
use Auth;

class SettingController extends Controller
{
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.setting.index');
    }
    public function datatable(Request $request) {
        $record = Setting::where("id",">",0);
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();
        $this->validate($request, [
            'key' => 'required',
            'value' => 'required',
            'status' => 'required',
        ]);


        $requestData = $request->all();
       
        $module = Setting::create($requestData);
        if($module){
            $result['message'] = \Lang::get('comman.responce_msg.item_created_success',['item'=>"Unit Package"]);
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);

    }
    public function edit($id)
    {
        $result = array();
        $item = Setting::findOrFail($id);
        
        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);

    }

    public function update($id, Request $request)
    {
        $result = array();

        $this->validate($request, [
            'key' => 'required',
            'value' => 'required',
            'status' => 'required',
        ]);

        $item = Setting::where("id",$id)->first();
        $requestData = $request->all();
        
        if($item){
            $item->update($requestData);
            $result['message'] = \Lang::get('comman.responce_msg.record_updated_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/setting');
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Setting::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/setting');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $setting = Setting::findOrFail($id);
        
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $setting->status= 'inactive';
                $setting->update();  
                return redirect()->back();
            }else{
                $setting->status= 'active';
                $setting->update();               
                return redirect()->back();
            }
        }
        return view('admin.setting.show', compact('setting'));
    }
}
