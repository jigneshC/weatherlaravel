<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Packages;
use App\FbuserDB;
use App\Billing;
use App\User;
use App\UnitTransaction;
use App\AdsetDB;
use App\CampaignDB;
use App\FbadDB;
use App\Temprature;
use Session;
use Auth;
use Carbon;
use App\Logic\ApiCall;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;


use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccountUser;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdAccountUserFields;

use App\Http\Controllers\EmailController;
class CompaignsController extends Controller
{
    private $mail_function;
    public function __construct()
    {
       $this->mail_function = new EmailController();
    }
   
    public function MyFbCompains(Request $request)
    {
        
        $perPage = 10;
        
        $campaigns = CampaignDB::whereIn('account_id',Session::get('account_ids'))->orderby("status", "ASC")
                ->paginate($perPage);
        
        return view('user-backend.compaigns.fbcompaigns',  compact('campaigns','getMaxtemp'));
     
    }
    
    public function MyFbCompainView($campaign_id,Request $request)
    {
        $perPage = 10;
        
         $campaign = CampaignDB::where('campaign_id',$campaign_id)->whereIn('account_id',Session::get('account_ids'))->first();
         
         if(!$campaign){
             Session::flash('flash_error',"Something went wrong !!");
             return redirect('fb-compaigns');
         } 
        
        
        
        $ads = FbadDB::where('campaign_id',$campaign_id)
                ->whereIn('account_id',Session::get('account_ids'))
                ->orderby("status","ASC")
                ->paginate($perPage);
        
        
        
                
        return view('user-backend.compaigns.fbcompaigns-detail',  compact('ads','campaign'));
    }
    public function MyFbAdsets(Request $request)
    {
        $campaigns = CampaignDB::whereIn('account_id',Session::get('account_ids'))->orderby("status","ASC")->pluck('name', 'campaign_id')->prepend('Select Compaign', '');
        return view('user-backend.compaigns.fb-adsets',  compact('campaigns'));
    }
    public function MyFbAdsetsEdit($id,Request $request)
    {
        $item = FbadDB::where('id',$id)->whereIn('account_id',Session::get('account_ids'))->first();
        
        if($item){
            $loc = null;
            if($item->adsets){
                $loc = json_decode($item->adsets->geo_locations,true);
            }
            
            if($loc && isset($loc['custom_locations']) && ($item->location_longitude=="" || !$item->location_longitude)){
                if(isset($loc['custom_locations'][0]) && isset($loc['custom_locations'][0]['longitude']) ){
                    $item->location_longitude = $loc['custom_locations'][0]['longitude'];
                }
                if(isset($loc['custom_locations'][0]) && isset($loc['custom_locations'][0]['latitude']) ){
                    $item->location_latitude = $loc['custom_locations'][0]['latitude'];
                }
                if(isset($loc['custom_locations'][0]) && isset($loc['custom_locations'][0]['country']) ){
                    $item->location_countrycode = $loc['custom_locations'][0]['country'];
                }
            }else if($loc && isset($loc['cities']) && ($item->location_city=="" || !$item->location_city) ){
                if(isset($loc['cities'][0]) && isset($loc['cities'][0]['name']) ){
                    $item->location_city = $loc['cities'][0]['name'];
                    $item->location_name = $loc['cities'][0]['name'];
                }
                if(isset($loc['cities'][0]) && isset($loc['cities'][0]['region']) ){
                    $item->location_regions = $loc['cities'][0]['region'];
                }
                if(isset($loc['cities'][0]) && isset($loc['cities'][0]['country']) ){
                    $item->location_countrycode = $loc['cities'][0]['country'];
                }
            }else if($loc && isset($loc['regions']) && ($item->location_city=="" || !$item->location_city) ){
                if(isset($loc['regions'][0]) && isset($loc['regions'][0]['name']) ){
                    $item->location_city = $loc['regions'][0]['name'];
                }
                if(isset($loc['regions'][0]) && isset($loc['regions'][0]['country']) ){
                    $item->location_countrycode = $loc['regions'][0]['country'];
                }
            }else if($loc && isset($loc['countries']) ){
                if(isset($loc['countries'][0]) ){
                    $item->location_countrycode = $loc['countries'][0];
                }
            }
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);
    }
    public function updateFbAdsets($id, Request $request)
    {
        $res = 0;
        $result = array();
        $requestData = $request->except('adset_id');
        
        if($request->has('adset_ids') && $request->adset_ids!=""){
            
            $adset_ids = explode(",",$request->adset_ids);
            foreach ($adset_ids as $ida) {
                $item = FbadDB::where('id',$ida)->whereIn('account_id',Session::get('account_ids'))->first(); 
                if($item){
                    $res = $item->update($requestData);
                }
            }
        }else{
           $item = FbadDB::where('id',$id)->whereIn('account_id',Session::get('account_ids'))->first(); 
           if($item){
               $res = $item->update($requestData);
           }
        }
        
        
        if($res){
            $result['message'] = \Lang::get('comman.responce_msg.record_updated_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/packages');
        }
        
    }
    public function updateFbAd($id, Request $request)
    {
        $res = 0;
        $result = array();
        $requestData = $request->except('adset_id');
        
        $item = FbadDB::where('id',$id)->whereIn('account_id',Session::get('account_ids'))->first();
        
        if($request->has('status') && $item && ( $request->status=="ACTIVE" || $request->status=="PAUSED" )){
            Api::init(config('syncaccount.fb.FACEBOOK_APP_ID'),config('syncaccount.fb.FACEBOOK_APP_SECRET'),Session::get('fb_access_token'));
            
            $ad = new Ad($item->ad_id);
            $ad->status = $request->status; //ACTIVE,PAUSED,DELETED,ARCHIVED
            $ad->save();
            
            $item->status = $request->status;
            $item->save();
            
            $result['message'] = "Facebook Ad status change to ".$request->status;
            $result['code'] = 200;
            
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');;
            $result['code'] = 400;
        }
        
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect()->back();
        }
        
    }
    
    public function DataTableAdsets(Request $request)
    {
        
        $adsets = FbadDB::whereIn('account_id',Session::get('account_ids'));
        
        if($request->has('campaign_id')){
            $adsets->where('campaign_id',$request->campaign_id);
            $adsets->orderby("campaign_id","ASC");
        }else{
            $adsets->orderby("status","ASC");
        }
        
        return Datatables::of($adsets)->make(true);
    }
   
    


}
