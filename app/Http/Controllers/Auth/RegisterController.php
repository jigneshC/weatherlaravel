<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Session;
use Illuminate\Support\Facades\Lang;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'activation_token' => sha1(time() . uniqid() . $data['email']),
            'activation_time' => \Carbon\Carbon::now(),
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());

        event(new Registered($user));

        $this->afterRegister($user);

        
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('flash_success', 'Registration Successful. Please check your email for activation link');
    }
    public function afterRegister($user)
    {
        $this->sendEmail($user);
    }


    /**
     * Send Email Helper
     *
     * @param User $user
     */
    public function sendEmail(User $user)
    {
        return $user->notify(new  \App\Notifications\ActivationLink($user));
    }

    public function activateAccount($token)
    {

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            Session::flash('flash_error', 'This link is expired.');
            return redirect('/');
        }

       /* $stripe = Stripe::make(env("STRIPE_SECRET"));
        $customer = $stripe->customers()->create([
             'email' => $user->email,
        ]);

        if($customer && isset($customer['id'])){
            $user->customer_stripe_id = $customer['id'];
        }*/
        $user->activation_token = null;
        $user->activation_time = null;
        $user->is_active = 1;
        $user->save();


        if (!$this->guard()->check()) {
           // $this->guard()->login($user);
        }

        return redirect()->intended($this->redirectPath())->with('flash_success', 'Your account has been activated.');

    }
    public function resendActivationEmail(Request $request)
    {
        return view('auth.passwords.resendmail');
    }
    public function resendActivationEmailToUser(Request $request)
    {


         $user = User::where('email', $request->email)->first();
        
        if($user && $user->is_active == 0){
            $user->activation_token = sha1(time() . uniqid() .$request->email);
            $user->save();

         $this->sendEmail($user);
            return back()->with('flash_success', 'We resent you account activation email. Please check your email inbox.');
        }else{
            return back()->with('flash_error', 'No user Found.');
        }
      
    }
}
