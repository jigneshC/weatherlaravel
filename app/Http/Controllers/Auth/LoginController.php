<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;
use Session;
use Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/Dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function authenticate(Request $request)
    {
        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'is_active' => 1
        );

        $user = User::where("email", $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {

                if ($user->is_active == 1 && Auth::attempt($credentials)) {
                    
                    User::setUserSession();
                    Session::flash('flash_success',"Login Success !!");
                    return redirect()->intended($this->redirectPath());
                } else {
                    Session::flash('flash_warning',"Your Account Has Not Activated Yet");
                    Session::flash('is_active_error', 'Yes');
                }
            } else {
                Session::flash('flash_error',"Invalid Username or Password");
            }
        } else {
            Session::flash('flash_error',"Invalid Username or Password");
        }
       

        return redirect()->back()->withInput();

    }
}
