<?php

namespace App\Http\Controllers;

use App\Adsettings;
use Illuminate\Http\Request;


use App\Packages;
use App\FbuserDB;
use App\Billing;
use App\User;
use App\UnitTransaction;
use App\AdsetDB;
use App\CampaignDB;
use App\FbadDB;
use App\Temprature;


use Session;
use Auth;
use Carbon;


class AdsettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignSettings($id, Request $request)
    {
        
       
       
      /*  $q = "DELETE FROM `migrations` WHERE `migration` = '2018_04_14_101849_create_temprature_table'";
         \DB::statement($q);
         
         $q = "DROP TABLE temprature";
         \DB::statement($q);
         exit;*/
        
        $fbad = FbadDB::where('ad_id',$id)->whereIn('account_id',Session::get('account_ids'))->first();
        if(!$fbad){
            Session::flash('flash_error',"Something went wrong !!");
            return redirect('fb-compaigns');
        }
        $adsetting = Adsettings::where('ad_id',$fbad->id)->first();
    //     echo "<pre>";      print_r($fbad); exit;
    //      echo "<pre>";      print_r($fbad->adsettings); exit;
     // echo $adsetting->getNextCheckTime(); exit;
         
        return view('user-backend.compaigns.fbad-settings',  compact('fbad','adsetting'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->except("day_of_week");
        
        $this->validate($request, [
            'min_day' => 'required',
            'ad_id' => 'required',
            'show_or_not' => 'required',
            'weather_type' => 'required',
            'qualifier' => 'required',
        ]);
        
        $requestData['status'] = "active";
        $requestData['hoursofday_from'] = $requestData['hoursofday_from'];
        $requestData['hoursofday_to'] = $requestData['hoursofday_to'];
        $requestData['day_of_week'] = json_encode($request->day_of_week);
        $requestData['setting_end_time'] = \Carbon\Carbon::now()->addDays($request->min_day);
        $fbad = FbadDB::where('id',$request->ad_id)->whereIn('account_id',Session::get('account_ids'))->first();
        if(!$fbad){
            Session::flash('flash_error',"Something went wrong !!");
            return redirect('fb-compaigns');
        }
        $adsetting = Adsettings::where('ad_id',$request->ad_id)->first();
        if($adsetting){
            Session::flash('flash_success',"Setting updated !!");
            $adsetting->update($requestData);
        }else{
            Session::flash('flash_success',"Setting Created !!");
            $adsetting = Adsettings::create($requestData);
        }
       
        $adsetting = Adsettings::where('ad_id',$request->ad_id)->first();
       
        $fbad->next_check_time = $adsetting->getNextCheckTime();
        $fbad->save();
                
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Adsettings  $adsettings
     * @return \Illuminate\Http\Response
     */
    public function show(Adsettings $adsettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Adsettings  $adsettings
     * @return \Illuminate\Http\Response
     */
    public function edit(Adsettings $adsettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adsettings  $adsettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Adsettings $adsettings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adsettings  $adsettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adsettings $adsettings)
    {
        //
    }
}
