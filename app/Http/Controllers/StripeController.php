<?php

namespace App\Http\Controllers;


use Auth;
use Carbon;
use App\Setting;
use App\Package_User;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class StripeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user())
        {
            $id = Auth::user()->id;
            $userRecord = Auth::user();
            $packageData = Package_User::where('user_id',$id)->first();
            if($packageData)
            {
                $createdDate = $userRecord->created_at;        
                $settingFreeTrialDayRecord = Setting::select('value','id','status','key')->where('key','free_trial_days')->first();
                $freeTrialDyas = (int)$settingFreeTrialDayRecord->value;
            
                $updated_at=Carbon::parse( $createdDate)->addDays($freeTrialDyas);
                $lengthOfAd = $updated_at->diffInDays(Carbon::now());
                if($lengthOfAd == 0){
                    Session::flash('flash_error', 'Please Buy new plan.your free trial has been expire!');  
                }
                
            }
        }
        
        
        return view('frontend.home');
    }
    public function plan()
    {
        return view('frontend.plan');
    }
    public function learnMore()
    {
        return view('frontend.learn-more');
    }
    public function support()
    {
        return view('frontend.support');
    }

    public function stripe(){
        // \Stripe\Stripe::setApiKey("sk_test_99gZgjwIk0zD8Feiuv8hFFPN");

        // $charge = \Stripe\Charge::create([
        //     'amount' => 999,
        //     'currency' => 'usd',
        //     'source' => 'tok_visa',
        //     'receipt_email' => 'jenny.rosen@example.com',
        // ]);

        // echo $charge->getLastResponse()->headers['Request-Id'];

        return view('frontend.paywithstripe');

    }

    public function createPlan(){
        $stripe = Stripe::make(env("STRIPE_SECRET"));
        $plan = $stripe->plans()->create([
            'id' => 'monthly',
            'name' => 'Monthly (30$)',
            'amount' => 30.00,
            'currency' => 'USD',
            'interval' => 'month',
            'statement_descriptor' => 'Monthly Subscription',
        ]);

        echo $plan['id'];
         echo $plan['product'];
        exit;
    }

    public function makeSubscription()
    {
        $stripe = Stripe::make(env("STRIPE_SECRET"));
        //Create token if customer does not assign any credit card
        $token = $stripe->tokens()->create([
            'card' => [
                'number' => '4242424242424242',
                'exp_month' => 10,
                'cvc' => 314,
                'exp_year' => 2020,

            ],
        ]);
        // assign token to customer 
        $card = $stripe->cards()->create('cus_CXpRCccK7LZz7D', $token['id']);

        //Link Subscription
        $subscription = $stripe->subscriptions()->create('cus_CXpRCccK7LZz7D', [
            'plan' => 'monthly',
        ]);

        echo $subscription['id'];

        exit;
    }

    public function postPaymentWithStripe(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'card_no' => 'required',
            'ccExpiryMonth' => 'required',
            'ccExpiryYear' => 'required',
            'cvvNumber' => 'required',
            'amount' => 'required',
        ]);
        $userid = auth()->user()->id;
        $user = User::find($userid);
        $input = $request->all();
        if ($validator->passes()) {
            $input = array_except($input, array('_token'));
            // Stripe Secret Key
            $stripe = Stripe::make(env("STRIPE_SECRET"));
            try {
                $cusId = 'cus_CXpRCccK7LZz7D';
                // $customer = $stripe->customers()->create([
                //     'email' => $user->email,
                // ]);

                // echo $customer['id'];
                
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number' => $request->get('card_no'),
                        'exp_month' => $request->get('ccExpiryMonth'),
                        'exp_year' => $request->get('ccExpiryYear'),
                        'cvc' => $request->get('cvvNumber')
                        
                    ],
                ]);
                if (!isset($token['id'])) {
                    \Session::put('error', 'The Stripe Token was not generated correctly');
                    return redirect()->route('stripform');
                }
                echo '<pre>';
                print_r($token);
                echo '</pre>';
                exit;
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'customer' => $cusId,
                    'currency' => 'USD',
                    'amount' => $request->get('amount'),
                    'description' => 'Add in wallet',
                ]);
                if ($charge['status'] == 'succeeded') { 
                    /**
                     * Write Here Your Database insert logic.
                     */

                    echo '<pre>';
                    print_r($charge);
                    echo '</pre>';exit;
                    \Session::put('success', 'Money add successfully in wallet');
                    return redirect()->route('stripform');
                } else {
                    \Session::put('error', 'Money not add in wallet!!');
                    return redirect()->route('stripform');
                }
            } catch (Exception $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripform');
            } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripform');
            } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripform');
            }
        }
        \Session::put('error', 'All fields are required!!');
        return redirect()->route('stripform');
    }

}
