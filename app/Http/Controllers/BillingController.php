<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Packages;
use App\FbuserDB;
use App\Billing;
use App\User;
use App\UnitTransaction;

use Session;
use Auth;
use Carbon;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

use App\Http\Controllers\EmailController;
class BillingController extends Controller
{
    private $mail_function;
    public function __construct()
    {
       $this->mail_function = new EmailController();
    }
   
    public function plans(Request $request)
    {
        $this->checkBillingCycle();
       $packages = Packages::where("stripe_product_id","!=",null)->orderBy("price","ASC")->get();
       return view('user-backend.billing.plans',compact('packages'));
    }
    
    public function subscription(Request $request)
    {
       $current_subscription = Billing::where("billing_status","active")->where("user_id",Auth::user()->id)->first();
       $count_subscription = Billing::where("user_id",Auth::user()->id)->count();
       return view('user-backend.billing.subscription',compact('current_subscription','count_subscription'));
    }
    
    
    public function doSubscriptionTrial(Request $request)
    {
       $requestData = [];
       $requestData['user_id']=Auth::user()->id;
       $requestData['unit']=\config('settings.free_trial_pack_info.unit');
       $requestData['buyer_name']=Auth::user()->name;
       $requestData['buyer_email']=Auth::user()->email;
       $requestData['item_price']=0;
       $requestData['price_currency']="USD";
       $requestData['item_qty']=1;
       $requestData['discount']=0;
       $requestData['billing_type']="trial";
       $requestData['billing_status']="active";
       $requestData['cycle_start_date']= Carbon\Carbon::now();
       $requestData['expiry_date']=Carbon\Carbon::now()->addDays(\config('settings.free_trial_pack_info.FREE_TRIAL_DAY'))->endOfDay();
       $requestData['next_billing_date']=Carbon\Carbon::now()->addDays(\config('settings.free_trial_pack_info.FREE_TRIAL_DAY'))->endOfDay();
       $requestData['package']=  json_encode(\config('settings.free_trial_pack_info'));
       
       Billing::create($requestData);
       
       Session::flash('flash_success',"Your Free Trial Plan for ".\config('settings.free_trial_pack_info.FREE_TRIAL_DAY')." Activated");
       return redirect()->back();
       
       
    }
    
    public function unitHistory(Request $request)
    {
       $packages = Packages::where("id",">",0)->orderBy("price","ASC")->get();
       return view('user-backend.billing.unit-history',compact('packages'));
    }
    public function history(Request $request)
    {
       $packages = Packages::where("id",">",0)->orderBy("price","ASC")->get();
       return view('user-backend.billing.history',compact('packages'));
    }
   
    public function confirmOrder($package_id)
    {
       $package = Packages::whereId($package_id)->orderBy("status","active")->first();
       
       if(!$package){
            Session::flash('flash_error',"No Package Found");
            return redirect('Billing/plans');
       }
       return view('user-backend.billing.confirm-order',compact('package'));

    }
    public function confirmOrderPayment($order_id)
    {
       $billing = Billing::whereId($order_id)->where("billing_status","pending")->first();
       
       if(!$billing){
            Session::flash('flash_error',"Something went wrong !!");
            return redirect('Billing/plans');
       }
       return view('user-backend.billing.confirm-order-payment',compact('billing'));

    }
    public function doOrderStep1(Request $request)
    {
       
        $this->validate($request, [
                'buyer_name' => 'required',
                'buyer_email' => 'required',
                'package_id' => 'required',
        ]);
        
        $package = Packages::where("id",$request->package_id)->first();

        if(!$package){
            Session::flash('flash_message',"No Package Found");
            return redirect()->back();
        }
        
        $requestData = [];
        $requestData['user_id']=Auth::user()->id;
        $requestData['unit']=$package->unit;
        $requestData['buyer_name']=$request->buyer_name;
        $requestData['buyer_email']=$request->buyer_email;
        $requestData['item_price']=$package->price;
        $requestData['price_currency']=$package->currency;
        $requestData['plan_id']=$package->slug_id;
        $requestData['item_qty']=1;
        $requestData['discount']=0;
        $requestData['billing_type']="stripe";
        $requestData['billing_status']="pending";
        $requestData['cycle_start_date']= Carbon\Carbon::now(); 
        $requestData['next_billing_date']=Carbon\Carbon::now(); 
        $requestData['package']=  json_encode($package);

        $billing = Billing::create($requestData);
        return redirect('order/payment/'.$billing->id);
    }
    public function doOrderPayment(Request $request)
    {
       $this->validate($request, [
             'card_no' => 'required',
            'ccExpiryMonth' => 'required',
            'ccExpiryYear' => 'required',
            'cvvNumber' => 'required',
            'billing_id' => 'required',
        ]);
       
        
        $billing = Billing::where("id",$request->billing_id)->first();

        if(!$billing){
            Session::flash('flash_message',"Some thing went wrong !!");
            return redirect()->back();
        }
        //$package = json_decode($billing->package);
        
        $stripe = Stripe::make(env("STRIPE_SECRET"));
        
        $user = User::where('id',Auth::user()->id)->first();
        if($user->customer_stripe_id=="" || !$user->customer_stripe_id){
            $customer = $stripe->customers()->create([
                'email' => $user->email,
            ]);
            if($customer && isset($customer['id'])){
                $user->customer_stripe_id = $customer['id'];
                $user->save();
            }
        }
        if($user->customer_stripe_id==""){
            Session::flash('flash_message',"Stripe User not created , please try again later !!");
            return redirect()->back();
        }
        
        $card = $stripe->tokens()->create([
            'card' => [
                'number' => $request->get('card_no'),
                'exp_month' => $request->get('ccExpiryMonth'),
                'exp_year' => $request->get('ccExpiryYear'),
                'cvc' => $request->get('cvvNumber')
            ],
        ]);
        
        // Assign card to customer
        if($card && isset($card['id'])){
              $stripe->cards()->create($user->customer_stripe_id,$card['id']); 
        }
        
        //Link Subscription
        $subscription = $stripe->subscriptions()->create($user->customer_stripe_id, [
            'plan' => $billing->plan_id,
        ]);
        
        if($subscription && isset($subscription['id'])){
             $this->stopAllSubscriptionByUserId(Auth::user()->id);
             $billing->subscription_id =  $subscription['id'];
             $billing->customer_stripe_id =  $user->customer_stripe_id;
             $billing->billing_status =  "active";
             $billing->save();
        }else{
            Session::flash('flash_message',"Subscription not created , please try again later !!");
            return redirect()->back();
        }
        $this->mail_function->sendMailOnBillingStart($billing->id);
        $this->checkBillingCycle();  
        
        Session::flash('flash_success',"You Subscription started !!");
        return redirect('Billing/subscription');
        
        
    }

    public function stopAllSubscriptionByUserId($user_id)
    {
        $billings = Billing::where("user_id",$user_id)->where("billing_status","!=","expired")->where("billing_type","stripe")->get();
        foreach($billings as $billing){
            if($billing->subscription_id && $billing->subscription_id!=""){
                $stripe = Stripe::make(env("STRIPE_SECRET"));
                $subscription = $stripe->subscriptions()->cancel($billing->customer_stripe_id,$billing->subscription_id);
            }
        }
        Billing::where("user_id",$user_id)->update(["billing_status"=>"expired"]);
    }
    public function checkBillingCycle()
    {
        $stripe = Stripe::make(env("STRIPE_SECRET"));
        
        Billing::where("billing_status","active")->where("next_billing_date","<=",\Carbon\Carbon::now())->update(["billing_status"=>"suspended"]);
        
        $billings = Billing::where("billing_status","!=","expired")->where("billing_status","!=","pending")->where("next_billing_date","<=",\Carbon\Carbon::now()->addHour(12))->get();
        foreach($billings as $billing){
            $package = json_decode($billing->package);
            
            $invoices = $stripe->invoices()->all(["subscription"=>$billing->subscription_id]);
            if(isset($invoices['data'])){
                 foreach($invoices['data'] as $invoice){
                     $p_start_time =  \Carbon\Carbon::now()->startOfDay();
                     
                     if($package->interval=="week"){
                         $p_end_time =  \Carbon\Carbon::now()->addWeek()->startOfDay();
                     }else if($package->interval=="month"){
                         $p_end_time =  \Carbon\Carbon::now()->addMonth()->startOfDay();
                     }else if($package->interval=="year"){
                         $p_end_time =  \Carbon\Carbon::now()->addYear()->startOfDay();
                     }
                     
                     
                     $transaction = UnitTransaction::where("invoice_id",$invoice['id'])->first();
                     if(!$transaction && $invoice['amount_remaining']==0){
                         $transaction = new UnitTransaction();
                         $transaction->user_id = $billing->user_id;
                         $transaction->unit_type = "credit";
                         $transaction->unit = $billing->unit;
                         $transaction->reference = "stripe_credit";
                         $transaction->billing_id = $billing->id;
                         $transaction->invoice_id = $invoice['id'];
                         $transaction->charge_id = $invoice['charge'];
                         $transaction->invoice_object = json_encode($invoices['data']);
                         
                         $transaction->period_start_date = $p_start_time;
                         $transaction->period_end_date = $p_end_time;
                         $transaction->save();
                         
                         if(\Carbon\Carbon::now() < $p_end_time){
                             $billing->billing_status = "active";
                             $billing->last_billing_date = $p_start_time;
                             $billing->next_billing_date = $p_end_time;
                             $billing->expiry_date = $p_end_time;
                             $billing->save();
                         }
                         
                         $this->mail_function->sendMailOnInvoiceGenerate($transaction->id);
                     }
                 }
            }
            
        }
        
    }

    


}
