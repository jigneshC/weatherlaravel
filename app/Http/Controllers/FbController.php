<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FbuserDB;
use App\AdaccountDB;
use App\CampaignDB;
use App\AdsetDB;
use App\FbadDB;

use Auth;
use Session;

use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccountUser;
use FacebookAds\Object\Fields\AdAccountUserFields;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;


class FbController extends Controller {

    
    
    public function sync(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {
        
        $fb_sync_status = 0;
        if(Session::has('fb_access_token') && Session::get('fb_access_token')!=""){
            $app_id = config('syncaccount.fb.FACEBOOK_APP_ID');
            $app_secret = config('syncaccount.fb.FACEBOOK_APP_SECRET');
            $access_token = Session::get('fb_access_token');
            $fb_id = Session::get('fb_id');
            
            
            
            try{
            Api::init($app_id, $app_secret, $access_token);    
            $user = new AdAccountUser($fb_id);
            $user->read(array(AdAccountUserFields::ID));
            
            $fb_sync_status = 1;
            }catch (\Exception $e){
                Session::forget('fb_access_token_start_time');
                Session::forget('fb_access_token');
                Session::forget('fb_id');
                
                $errorcode = $e->getCode();
                $error_msg = $e->getMessage();
                $fb_sync_status = 0;
                Session::flash('flash_error',$error_msg);
            }
        }
        
        $login_url = $fb->getLoginUrl(['public_profile,email,ads_management,manage_pages,business_management']);
        return view('frontend.socialsync',compact('fb_sync_status','login_url'));
    }
    public function callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {
        
        // Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
             $error = $e->getMessage();
        }
        
        if (!$token) {
            $error = "";
        }
        
         
        if (1) {
            $oauth_client = $fb->getOAuth2Client();
           
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                $error = $e->getMessage();
            }
        }else{
            
        }
       
         $fb->setDefaultAccessToken($token);
         
         echo $token;
         
         
    }
    
    public function index() {
        
        if (Session::has('fb_access_token') && Session::get('fb_access_token') != "") {
            
            $fb_id = Session::get('fb_id');
            Api::init(config('syncaccount.fb.FACEBOOK_APP_ID'),config('syncaccount.fb.FACEBOOK_APP_SECRET'),Session::get('fb_access_token'));
            
            $account = new AdAccount("act_344161492");
            

           
        } else {
            return view('frontend.fb.index');
        }
    }

    
   

    public function savedetails() {
        Session::put('fb_id', $_POST['id']);
        Session::put('fb_access_token', $_POST['accesstoken']);
        Session::put('fb_access_token_start_time', \Carbon\Carbon::now());


        $fb_user = FbuserDB::where('fb_id', $_POST['id'])->where('user_id', Auth::user()->id)->first();
        if (!$fb_user) {
            $data['name'] = $_POST['name'];
            $data['fb_id'] = $_POST['id'];
            $data['user_id'] = Auth::user()->id;
            $fbuser = FbuserDB::create($data);
        }
        $accesstoken = $_POST['accesstoken'];
        


        /*         * *Fetch Ad account** */
        $url = "https://graph.facebook.com/v2.12/" . $_POST['id'] . "/adaccounts?access_token=" . $accesstoken;

        $resultArray = $this->curl_call($url);

        if (count($resultArray) > 0) {
            foreach ($resultArray->data as $res) {
                $adaccounts = AdaccountDB::where('act_id', $res->id)->where('user_id', Auth::user()->id)->first();
                if (!$adaccounts) {
                    $adata['act_id'] = $res->id;
                    $adata['fb_id'] = $_POST['id'];
                    $adata['user_id'] = Auth::user()->id;
                    
                    $adaccount = AdaccountDB::create($adata);
                    $Aurl = "https://graph.facebook.com/v2.12/" . $res->id . "/campaigns";
                    $AdUrl = "https://graph.facebook.com/v2.12/" . $res->id . "/adsets";
                } else {
                    $Aurl = "https://graph.facebook.com/v2.12/" . $adaccounts->act_id . "/campaigns";
                    $AdUrl = "https://graph.facebook.com/v2.12/" . $adaccounts->act_id . "/adsets";
                }

                $Aurl .= "?fields=name%2Caccount_id%2Cstatus%2Ccreated_time&access_token=" . $accesstoken;
                $AdUrl .= "?fields=name%2Ccampaign_id%2Cstatus%2Ccreated_time&access_token=" . $accesstoken;

                $campaignArray = $this->curl_call($Aurl);

                foreach ($campaignArray->data as $ca) {
                    $campaignexist = CampaignDB::where('campaign_id', $ca->id)->where('user_id', Auth::user()->id)->first();
                    if (!$campaignexist) {
                        $cdata['act_id'] = $res->id;
                        $cdata['fb_id'] = $_POST['id'];
                        $cdata['user_id'] = Auth::user()->id;
                        $cdata['name'] = $ca->name;
                        $cdata['status'] = $ca->status;
                        $cdata['created_time'] = $ca->created_time;
                        $cdata['campaign_id'] = $ca->id;
                        $campaign = CampaignDB::create($cdata);
                    } else if ($campaignexist && $campaignexist->status != $ca->status) {
                        $cdata->status = $ca->status;
                        $campaignexist->update($cdata);
                    }
                }

                $AdArray = $this->curl_call($AdUrl);

                foreach ($AdArray->data as $ad) {
                    $Adexist = AdsetDB::where('ad_id', $ad->id)->where('user_id', Auth::user()->id)->first();
                    if (!$Adexist) {
                        $addata['ad_id'] = $ad->id;
                        $addata['act_id'] = $res->id;
                        $addata['fb_id'] = $_POST['id'];
                        $addata['user_id'] = Auth::user()->id;
                        $addata['name'] = $ad->name;
                        $addata['status'] = $ad->status;
                        $addata['created_time'] = $ad->created_time;
                        $addata['campaign_id'] = $ad->campaign_id;
                        $Adset = AdsetDB::create($addata);
                    } else if ($Adexist && $Adexist->status != $ad->status) {
                        $addata->status = $ad->status;
                        $Adexist->update($addata);
                    }
                }
            }
        }
        $result = [];
        $result['message'] = "Success";
        $result['code'] = 200;
        
        Session::flash('flash_success',"Facebook Account Sync successfully !!");
        return response()->json($result, $result['code']);
    }

    

    public function curl_call($url) {
        $session = curl_init($url);
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );
        curl_setopt($session, CURLOPT_URL, $url);
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($session);
        $resultArray = json_decode($result);
        // echo '<pre>';print_r($resultArray);exit;
        curl_close($session);
        return $resultArray;
    }

}
