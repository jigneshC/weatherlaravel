<?php

namespace App\Http\Controllers;


use Auth;
use Carbon;
use App\Setting;
use App\Package_User;
use App\Packages;

use App\AdsetDB;
use App\CampaignDB;
use App\FbadDB;

use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('themeb.home');
    }
    public function pricing()
    {
        $packages = Packages::where('status','active')->get();
        
        return view('themeb.pricing',compact('packages'));
    }
    public function features()
    {
        return view('themeb.features');
    }
    public function contact()
    {
        return view('themeb.contact');
    }
    public function faq()
    {
        return view('themeb.faq');
    }
    public function terms()
    {
        return view('themeb.terms');
    }
    
}
