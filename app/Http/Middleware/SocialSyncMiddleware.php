<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

use FacebookAds\Api;
use FacebookAds\Object\AdAccountUser;
use FacebookAds\Object\Fields\AdAccountUserFields;

class SocialSyncMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('fb_access_token') && Session::get('fb_access_token')!=""){
            
            return $next($request);
            
           /* $app_id = config('syncaccount.fb.FACEBOOK_APP_ID');
            $app_secret = config('syncaccount.fb.FACEBOOK_APP_SECRET');
            $access_token = Session::get('fb_access_token');
            $fb_id = Session::get('fb_id');
            
            $fb_access_token_start_time = Session::get('fb_access_token_start_time');
            $fb_expiry_minuts = config('syncaccount.settings.FB_TOKEN_EXPIRY');
            
            
           $created_at =  \Carbon\Carbon::parse($fb_access_token_start_time)->addMinutes($fb_expiry_minuts);
           $now= \Carbon\Carbon::now();
            
            if($created_at > $now){
                return $next($request);
            }
            
            
            try{
            Api::init($app_id, $app_secret, $access_token);    
            $user = new AdAccountUser($fb_id);
            $user->read(array(AdAccountUserFields::ID));
            }catch (\Exception $e){
                Session::forget('fb_access_token_start_time');
                Session::forget('fb_access_token');
                Session::forget('fb_id');
                
                $errorcode = $e->getCode();
                $error_msg = $e->getMessage();
                
                Session::flash('flash_error',$error_msg);
                return redirect('/social-sync');
            }*/
        }
        Session::flash('flash_error',"Please Login With Facebook And Sync Your Ad Account ...");
        return redirect('/social-sync');
    }
}
