<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use Carbon;
use App\Billing;

use App\AdaccountDB;
use App\CampaignDB;
use App\AdsetDB;
use App\FbadDB;

class SessionNotificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('session_notification_set_time') && Session::get('session_notification_set_time')!=""){
            $start_time = Session::get('session_notification_set_time');
            $created_at =  \Carbon\Carbon::parse($start_time)->addMinutes(30);
            $now= \Carbon\Carbon::now();
            
            if($created_at > $now){
                return $next($request);
            }
        }
        
        $this->setSessionNotification();
        
        return $next($request);
    }
    public function setSessionNotification(){
        $notification_befor_day = 15;
        $current_subscription = Billing::where("billing_status","active")->where("user_id",Auth::user()->id)->first();
        $count_subscription = Billing::where("user_id",Auth::user()->id)->count();
        
        $h1 = "";
        $h2 = "";
        $current_plan_name = "";
       
        if($count_subscription<=0){
            $h1 = \Lang::get('comman.text_line.notification_no_plan');
        }else if($current_subscription && $current_subscription->billing_type=="trial"){
            $expdt = Carbon::parse($current_subscription->expiry_date);
            $exp_in_day = ($expdt->diffInDays() ==0)? "Today": "In ".$expdt->diffInDays(). " day";
            if($expdt->diffInDays()<=$notification_befor_day){
                $h1 = \Lang::get('comman.text_line.notification_trial_plan_will_end_in_check_more_plan',['in_number_days'=>$exp_in_day]);
            }
            $h2 = \Lang::get('comman.text_line.notification_trial_plan_will_end_in',['in_number_days'=>$exp_in_day]);
        }else if($current_subscription && $current_subscription->billing_type!="trial"){
            $expdt = Carbon::parse($current_subscription->expiry_date);
            $exp_in_day = ($expdt->diffInDays() ==0)? "Today": "In ".$expdt->diffInDays(). " day";
            
            if($expdt->diffInDays()<=$notification_befor_day){
                $h1 = \Lang::get('comman.text_line.notification_plan_will_auto_renew_in_check_more_plan',['in_number_days'=>$exp_in_day]);
            }
            $h2 = \Lang::get('comman.text_line.notification_plan_will_auto_renew_in',['in_number_days'=>$exp_in_day]);
            
            
        }
        
        Session::put('session_notification_header1',$h1);
        Session::put('session_notification_header2',$h2);
        Session::put('session_notification_set_time', \Carbon\Carbon::now());
        
        if($current_subscription){
            $package = json_decode($current_subscription->package);
            Session::put('current_plan_name',$package->name);
        }
    }
    
}
