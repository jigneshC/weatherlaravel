<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdaccountDB extends Model
{
    protected $table = 'adaccounts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'id', 'account_id', 'user_id','fb_id','name','accesstoken','accesstokenLongLive','token_expires_time','tokenlonglive_expires_time'
    ];
    
    public function adsets() {
        return $this->hasMany('App\AdsetDB', 'account_id', 'account_id');
    }
    public function ads() {
        return $this->hasMany('App\FbadDB', 'account_id', 'account_id');
    }
}
