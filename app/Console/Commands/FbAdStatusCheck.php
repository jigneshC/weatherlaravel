<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\AdsetDB;
use App\AdaccountDB;
use App\Temprature;
use App\Logic\ApiCall;
use App\FbadDB;

use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccountUser;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdAccountUserFields;

use Illuminate\Support\Facades\File;
class FbAdStatusCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fbadstatus:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $uc = 0;
         $cron_gap = 300;
         $now_time = \Carbon\Carbon::now();
         FbadDB::where('next_check_time',">",\Carbon\Carbon::now()->subMinute($cron_gap))->where('next_check_time',"<=",$now_time)->update(['next_check_time'=>$now_time]);
         
        
        $fbads = FbadDB::where('next_check_time',$now_time)
                ->orderby("updated_at","DESC")->get();
                
        
        foreach ($fbads as $fbad) {
            $isMatch = false;
            $adaccount = $fbad->adaccount;
            if($adaccount && $adaccount->tokenlonglive_expires_time > $now_time){
                if($fbad->adsettings && $fbad->location_latitude){
                    
                   
                    $settings = $fbad->adsettings;
                    $temprature = ApiCall::getWeatherByLtLong($fbad->location_latitude,$fbad->location_longitude);
                   
                    if($temprature){
                        
                        if($settings->weather_type == "temparature" && $settings->qualifier && $settings->max_amount && $settings->min_amount){
                            $isMatch = $this->checkConditionMatch($settings->qualifier, $temprature->temp, $settings->min_amount, $settings->max_amount);
                        }
                        if($settings->weather_type == "humidity" && $settings->qualifier && $settings->max_amount && $settings->min_amount){
                            $isMatch = $this->checkConditionMatch($settings->qualifier, $temprature->humidity, $settings->min_amount, $settings->max_amount);
                        }
                        if($settings->weather_type == "pressure" && $settings->qualifier && $settings->max_amount && $settings->min_amount){
                            $isMatch = $this->checkConditionMatch($settings->qualifier, $temprature->pressure, $settings->min_amount, $settings->max_amount);
                        }
                        if($settings->weather_type == "wind" && $settings->wind && $settings->max_amount && $settings->min_amount){
                            $isMatch = $this->checkConditionMatchString($settings->qualifier, $temprature->wind, $settings->amount);
                        }
                        
                    }
                }
            
               
            }else{
                
            }
            if($isMatch){
                $uc ++;
                $newstatus = ($fbad->adsettings->show_or_not=="1" || $settings->show_or_not==1)? "ACTIVE" : "PAUSED" ;
                echo "<br/><br/> Condition Match | ".$this->updateFbAd($fbad, $fbad->adaccount,$newstatus)." | ".$fbad->name;
            }else{
                echo "<br/><br/> Condition Not Match  | ".$fbad->name;
            }
        }
        
        
        try{
            $filePath = public_path() . '/log';
            File::isDirectory($filePath) or File::makeDirectory($filePath, 0777, true, true);
            $filePath= $filePath."/cronlog.csv";
        
            $fp = fopen($filePath, 'a');    
            fputcsv($fp, array(\Carbon\Carbon::now()," fbadd check status =>".$uc));
            fclose($fp);
        }  catch (\Exception $e){
            
        }
                        
    }
    public function checkConditionMatchString($condition,$leftOp,$amount){
        $rightOp1 = 0;
        $rightOp2 = 1;
        if($leftOp=="wind" && ($amount=="none" || $amount=="light")){
            $rightOp1 = 0; $rightOp2 = 4;
        }else if($leftOp=="wind" &&  $amount=="moderate" ){
            $rightOp1 = 4; $rightOp2 = 15;
        }else if($leftOp=="wind" && $amount=="strong" ){
            $rightOp1 = 15; $rightOp2 = 30;
        }else if($leftOp=="wind"){
            $rightOp1 = 30; $rightOp2 = 3000;
        }
        
        $res = false;
        if($condition == "eq" ){
              if($leftOp == $rightOp1) {
                  $res = true;
              }       
        }
        if($condition == "gteq" ){
              if($leftOp >= $rightOp1) {
                  $res = true;
              }          
        }
        if($condition == "lteq" ){
              if($leftOp <= $rightOp1) {
                  $res = true;
              }          
        }
        if($condition == "between" ){
              if($leftOp >= $rightOp1 && $leftOp <= $rightOp2) {
                  $res = true;
              }          
        }
        if($condition == "outwith" ){
              if($leftOp >= $rightOp1 && $leftOp <= $rightOp2) {
                  
              }else{
                  $res = true;
              }          
        }
        return $res;
    }
    public function checkConditionMatch($condition,$leftOp,$rightOp1,$rightOp2){
        $res = false;
        if($condition == "eq" ){
              if($leftOp == $rightOp1) {
                  $res = true;
              }       
        }
        if($condition == "gteq" ){
              if($leftOp >= $rightOp1) {
                  $res = true;
              }          
        }
        if($condition == "lteq" ){
              if($leftOp <= $rightOp1) {
                  $res = true;
              }          
        }
        if($condition == "between" ){
              if($leftOp >= $rightOp1 && $leftOp <= $rightOp2) {
                  $res = true;
              }        
        }
        if($condition == "outwith" ){
              if($leftOp >= $rightOp1 && $leftOp <= $rightOp2) {
                  
              }else{
                  $res = true;
              }          
        }
        return $res;
    }
    
    public function updateFbAd($item,$adaccount,$newStatus)
    {
        
        try{
            
        
            if($item->status !=$newStatus){
                Api::init(config('syncaccount.fb.FACEBOOK_APP_ID'),config('syncaccount.fb.FACEBOOK_APP_SECRET'),$adaccount->accesstokenLongLive);

                $ad = new Ad($item->ad_id);
                $ad->status = $newStatus; //ACTIVE,PAUSED,DELETED,ARCHIVED
                $ad->save();

                $item->status = $newStatus;
                $item->save();

                return " Updated to ".$newStatus;

            }else{
                return " No change, its already :".$newStatus;
            }
        } catch (\Exception $e){
            return " Error : ".$e->getMessage();//"Your Facebook Account Have No Any Business Ad Account !!";
        }
    }
}
