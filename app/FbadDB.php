<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FbadDB extends Model
{
    protected $table = 'fb_ad';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'ad_id','name','fb_id','act_id','account_id','adset_id','campaign_id','status','created_time','max_temp','min_temp','location_longitude','location_latitude','location_name','location_city','location_postalcode','location_regions','location_country','location_countrycode','next_check_time','last_check_time'
    ];
    
    public function adsettings() {
        return $this->hasOne('App\Adsettings', 'ad_id', 'id');
    }
    
    public function adsets() {
        return $this->hasOne('App\AdsetDB', 'adset_id', 'adset_id');
    }
    public function adaccount() {
        return $this->belongsTo('App\AdaccountDB', 'account_id', 'account_id');
    }
    
    
}
