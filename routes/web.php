<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::get('/activate-account/{token}', 'Auth\RegisterController@activateAccount');
Route::get('/resend-activation', 'Auth\RegisterController@resendActivationEmail');
Route::post('/resend-activation', 'Auth\RegisterController@resendActivationEmailToUser');


Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/features', 'HomeController@features');
Route::get('/pricing', 'HomeController@pricing');
Route::get('/contact-us', 'HomeController@contact');

Route::get('/faq', 'HomeController@faq');
Route::get('terms-and-privacy', 'HomeController@terms');

// Route for
Route::group(['prefix' => 'admin','middleware' => ['auth', 'admin']], function () {
	
	Route::get('', 'Admin\AdminController@index');
	
	Route::resource('roles', 'Admin\RolesController');
	Route::resource('permissions', 'Admin\PermissionsController');
	Route::resource('users', 'Admin\UsersController');

        Route::get('packages/generate-stripe-plan/{id}', 'Admin\PackageController@generateStripePlan');
	Route::get('packages/datatable', 'Admin\PackageController@datatable');
	Route::resource('packages', 'Admin\PackageController');
	Route::post('packages/{id}', 'Admin\PackageController@update');
	
	Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

	Route::get('setting/datatable', 'Admin\SettingController@datatable');
    Route::resource('setting', 'Admin\SettingController');
});


//Route for logged in website
Route::group(['middleware' => ['auth']], function () {
    Route::get('/social-sync', 'FbController@sync');
    Route::post('/savedata', 'SocialSyncController@syncFacebookData');
    
    Route::get('/fblogin', 'FbController@index');
    Route::get('/facebook/callback', 'SocialSyncController@callback');
});

// Route for sinked with facebook or google user
Route::group(['middleware' => ['auth', 'social_sync']], function () {
 //   Route::get('/Dashboard', 'SocialSyncController@dashboard');
});


Route::group(['middleware' => ['auth','session_notification','social_sync']], function () {

Route::get('/Dashboard', 'SocialSyncController@dashboard');
Route::get('/Billing/plans', 'BillingController@plans');
Route::get('/Billing/subscription', 'BillingController@subscription');
Route::post('/Billing/subscription/trial', 'BillingController@doSubscriptionTrial');

Route::get('/Billing/unit-history', 'BillingController@unitHistory');
Route::get('/Billing/history', 'BillingController@history');

Route::get('/confirm-order/{id}', 'BillingController@confirmOrder');
Route::post('/confirm-order', 'BillingController@doOrderStep1');
Route::get('/order/payment/{id}', 'BillingController@confirmOrderPayment');
Route::post('/order/payment', 'BillingController@doOrderPayment');

Route::get('/profile', 'ProfileController@edit');
Route::post('/profile', 'ProfileController@update');
Route::get('/profile/change-password', 'ProfileController@changePassword')->name('profile.password');
Route::patch('/profile/change-password', 'ProfileController@updatePassword');



Route::get('fb-compaigns', 'CompaignsController@MyFbCompains');
Route::get('fb-compaigns/{id}', 'CompaignsController@MyFbCompainView');
Route::get('fb-adsets/datatable', 'CompaignsController@DataTableAdsets');
Route::get('fb-adsets', 'CompaignsController@MyFbAdsets');

Route::get('fb-adsets/{id}/edit', 'CompaignsController@MyFbAdsetsEdit');
Route::post('fb-adsets/{id}', 'CompaignsController@updateFbAdsets');
Route::put('fb-ad/{id}', 'CompaignsController@updateFbAd');




});
Route::get('fb-ad-settings/{id}', 'AdsettingsController@assignSettings');
Route::post('fb-ad-settings', 'AdsettingsController@store');



Route::get('/facebook-status-check', function(){
    Artisan::queue('fbadstatus:check');
});
