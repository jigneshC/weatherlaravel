<?php

$Qualifier1 = [
    'eq' => 'is',
    'gteq' => 'equal/greater than',
    'lteq' => 'equal/less than',
    'between' => 'between',
    'outwith' => 'outwith',
];
$Qualifier2 = [
    'eq' => 'is',
    'not' => 'is not',
    'gteq' => 'equal/greater than',
    'lteq' => 'equal/less than',
];

$amount1 = [
    'none' => 'None',
    'anyrain' => 'Light',
    'sigrain' => 'Moderate',
    'heavyrain' => 'Heavy',
];

$amount2 = [
    'none' => 'None',
    'anysnow' => 'Light',
    'sigsnow' => 'Moderate',
    'heavysnow' => 'Heavy',
];

$amount3 = [
    'sunny' => 'Sunny',
    'mostlysunny' => 'Mostly Sunny',
    'partsunny' => 'Part Sunny/Part Sunny',
    'mostlycloudy' => 'Mostly Sunny',
    'notsunny' => 'Not Sunny'
];
$amount4 = [
    'cloudy' => 'Cloudy',
    'mostlycloudy' => 'Mostly Cloudy',
    'partcloudy' => 'Part Cloudy/Part Clear',
    'mostlyclear' => 'Mostly Clear',
    'notcloudy' => 'Clear'
];
$amount5 = [
    'none' => 'None',
    'anyrain' => 'Light',
    'sigrain' => 'Moderate',
    'strong' => 'Strong',
    'verystrong' => 'Very Strong'
];

$time24hrs = [
    "00:00:00"=>"00:00",
    "01:00:00"=>"01:00",
    "02:00:00"=>"02:00",
    "03:00:00"=>"03:00",
    "04:00:00"=>"04:00",
    "05:00:00"=>"05:00",
    "06:00:00"=>"06:00",
    "07:00:00"=>"07:00",
    "08:00:00"=>"08:00",
    "09:00:00"=>"09:00",
    "10:00:00"=>"10:00",
    "11:00:00"=>"11:00",
    "12:00:00"=>"12:00",
    "13:00:00"=>"13:00",
    "14:00:00"=>"14:00",
    "15:00:00"=>"15:00",
    "16:00:00"=>"16:00",
    "17:00:00"=>"17:00",
    "18:00:00"=>"18:00",
    "19:00:00"=>"19:00",
    "20:00:00"=>"20:00",
    "21:00:00"=>"21:00",
    "22:00:00"=>"22:00",
    "23:00:00"=>"23:00",
];
return [
    'frequency_check_time'=>30,
    'fb' => [
        'FACEBOOK_APP_ID' => env('FACEBOOK_APP_ID', '193767031388583'),
        'FACEBOOK_APP_SECRET' => env('FACEBOOK_APP_SECRET', '4a48b1515e5f943cde71316ceabbf24f'),
    ],
    'google' => [
    ],
    'settings' => array(
        'FB_TOKEN_EXPIRY' => 30 // in minuts
    ),
    'time24hrs'=>$time24hrs,
    'day_range' => [
        'start' => [5],
        'connect' => true,
        'snap' => true,
        'range' => ['min' => 1,'14%' => 2,'21%' => 3,'28%' => 5,'35%' => 6,'42%' => 7,'49%' => 8,'56%' => 9,'63%' => 10,'70%' => 11,'77%' => 12,'84%' => 13,'91%' => 14,'max' => 15],
    ],
    'wfilters' => [
        'temparature' => [
            'qualifier' => $Qualifier1,
            'amount' => [
                'dropdown' => null,
                'rang' => ['range1' => true, 'range2' => true, 'unit' => '(℃)',
                    'obj' => [
                        'start' => [20, 40],
                        'connect' => true,
                        'margin' => 5,
                        'range' => ['min' => 0, 'max' => 80],
                    ]
                ]
            ]
        ],
        'rain' => [
            'qualifier' => $Qualifier2,
            'amount' => [
                'dropdown' => $amount1,
                'rang' => null
            ]
        ],
        'snow' => [
            'qualifier' => $Qualifier2,
            'amount' => [
                'dropdown' => $amount2,
                'rang' => null
            ]
        ],
        'sunny' => [
            'qualifier' => $Qualifier2,
            'amount' => [
                'dropdown' => $amount3,
                'rang' => null
            ]
        ],
        'cloud' => [
            'qualifier' => $Qualifier2,
            'amount' => [
                'dropdown' => $amount4,
                'rang' => null
            ]
        ],
        'wind' => [
            'qualifier' => $Qualifier2,
            'amount' => [
                'dropdown' => $amount5,
                'rang' => null
            ]
        ],
        'humidity' => [
            'qualifier' => $Qualifier1,
            'amount' => [
                'dropdown' => null,
                'rang' => ['range1' => true, 'range2' => true, 'unit' => '%',
                    'obj' => [
                        'start' => [25,30],
                        'connect' => true,
                        'range' => ['min' => 0, 'max' => 100],
                    ]
                ]
            ]
        ],
        'pressure' => [
            'qualifier' => $Qualifier1,
            'amount' => [
                'dropdown' => null,
                'rang' => ['range1' => true, 'range2' => true, 'unit' => 'mb',
                    'obj' => [
                        'start' => [900, 920],
                        'connect' => true,
                        'margin' => 25,
                        'range' => ['min' => 850, 'max' => 1100],
                    ]
                ]
            ]
        ],
        'uv' => [
            'qualifier' => $Qualifier1,
            'amount' => [
                'dropdown' => null,
                'rang' => ['range1' => true, 'range2' => true, 'unit' => '',
                    'obj' => [
                        'start' => [2,3],
                        'connect' => true,
                        'range' => ['min' => 0, 'max' => 11],
                    ]
                ]
            ]
        ],
    ],
];
