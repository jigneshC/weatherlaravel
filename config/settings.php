<?php

return [
    
    'GOOGLE_MAP_API' => env('GOOGLE_MAP_API', 'AIzaSyCpiWWaPaKhBbPSs2vZBECcZcvH2LUtj2U'),
    'FREE_TRIAL_DAY'=>env('FREE_TRIAL_DAY',15),
    'default_latitude'=>'28.68',
    'default_longitude'=>'77.22',
    'weather_key'=>[
        'WEATHER_API_KEY'=>env('WEATHER_API_KEY', 'd9225b5c77f36ce31e63d9a79dda4cd6'),
        'WEATHER_API_URL'=>'http://api.openweathermap.org/data/2.5/forecast',
    ],
    'package_class'=>[
        'STARTER'=>'blue-background',
        'BASIC'=>'orange-background',
        'PRO'=>'red-background',
        'ULTIMATE'=>'purple-background'
    ],
    
    'free_trial_pack_info'=>[
        'name'=>'Free Trial Package',
        'desc'=>'Free Trial account for 15 day.',
        'FREE_TRIAL_DAY'=>env('FREE_TRIAL_DAY',15),
        'unit'=>2,
    ],
    
    
    
   

];
