<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \DB::table('users')->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $user = User::create(
            [
                'name' => 'Jignesh',
                'email' => 'jignesh.citrusbug@gmail.com',
                'is_active'=>1,
                'password' => bcrypt(123456),
                'type' => "admin",
            ]
        );

        $user = User::create(
            [
                'name' => 'chetan',
                'email' => 'chetan.citrusbug@gmail.com',
                'is_active'=>1,
                'password' => bcrypt(123456),
                'type' => "admin",
            ]
        );

        $user = User::create(
            [
                'name' => 'snehal',
                'email' => 'snehal.citrusbug@gmail.com',
                'is_active'=>1,
                'password' => bcrypt(123456),
                'type' => "user",
            ]
        );


        


    }
}
