<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdaccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adaccounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('account_id')->nullable();
            $table->string('name')->nullable();
            $table->string('fb_id')->nullable();
            $table->text('accesstoken')->nullable();
            $table->text('accesstokenLongLive')->nullable();
            $table->dateTime('token_expires_time')->nullable();
            $table->dateTime('tokenlonglive_expires_time')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adaccounts');
    }
}
