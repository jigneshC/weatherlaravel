<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug_id')->nullable();
            $table->text('desc')->nullable()->default(null);
            $table->float('price')->default(0);
            $table->enum('currency', ['USD'])->default('USD');
            $table->integer('unit')->nullable()->default(null);
             
            $table->integer('interval_count')->nullable()->default(0);
            $table->enum('interval', ['week','month','year'])->default('week');
            $table->string('package_class')->nullable();
            $table->string('image')->nullable();
            $table->string('details')->nullable();
            $table->string('status')->nullable()->default('active');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            
            $table->text('stripe_product_id')->nullable()->default(null);
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}