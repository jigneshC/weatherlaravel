<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsettings', function (Blueprint $table) {
            $table->increments('id');
           
            
            $table->string('ad_id')->nullable();
            $table->string('status')->nullable();
            
            $table->float('min_day')->nullable()->default(null);
            $table->float('max_day')->nullable()->default(null);
            
            $table->float('min_amount')->nullable()->default(null);
            $table->float('max_amount')->nullable()->default(null);
            
             $table->tinyInteger('show_or_not')->nullable()->default(null);
             
             $table->string('weather_type')->nullable()->default(null);
             $table->string('qualifier')->nullable()->default(null);
             $table->string('amount')->nullable()->default(null);
             
             $table->string('frequency')->nullable()->default(null);
             $table->time('hoursofday_from')->nullable()->default(null);
             $table->time('hoursofday_to')->nullable()->default(null);
             $table->string('day_of_week')->nullable()->default(null);
             
             $table->tinyInteger('enable_frequency')->nullable()->default(null);
             $table->tinyInteger('enable_hoursofday')->nullable()->default(null);
             $table->tinyInteger('enable_day_of_week')->nullable()->default(null);
            
             $table->dateTime('setting_end_time')->nullable()->default(null);
             
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adsettings');
    }
}
