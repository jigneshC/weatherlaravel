<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsets', function (Blueprint $table) {
            $table->increments('id');
           
            
            $table->string('adset_id')->nullable();
            $table->string('campaign_id')->nullable();
            $table->string('account_id')->nullable();
            $table->string('fb_id')->nullable();
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->string('created_time')->nullable();
            $table->text('geo_locations')->nullable();
            
            $table->string('location_name')->nullable()->default(null);
            $table->string('location_city')->nullable()->default(null);
            $table->string('location_postalcode')->nullable()->default(null);
            $table->string('location_regions')->nullable()->default(null);
            $table->string('location_country')->nullable()->default(null);
            $table->string('location_countrycode')->nullable()->default(null);
            
            $table->string('location_longitude')->nullable()->default(null);
            $table->string('location_latitude')->nullable()->default(null);
            
            
            $table->float('min_temp')->nullable()->default(null);
            $table->float('max_temp')->nullable()->default(null);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adsets');
    }
}
