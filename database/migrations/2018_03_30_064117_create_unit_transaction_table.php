<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_transaction', function (Blueprint $table) {
            $table->increments('id');
	    $table->integer('user_id');
            $table->enum('unit_type', ['credit', 'debit'])->default('credit');
            $table->integer('unit')->default(0)->comment("Unit amount")->nullable();
            $table->enum('reference', ['self_debit','admin_credit','paypal_credit','stripe_credit']);
            $table->string('comment')->nullable()->default(null);
            
            $table->integer('billing_id')->nullable()->default(null);
            $table->string('invoice_id')->nullable()->default(null);
            $table->string('charge_id')->nullable()->default(null);
            
            $table->dateTime('period_start_date')->nullable()->default(null);
            $table->dateTime('period_end_date')->nullable()->default(null);
            
            $table->longText('invoice_object')->nullable()->default(null);
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_transaction');
    }
}
