<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserCardDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_card_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('card_id')->nullable();
            $table->integer('last4_digit_card');
            $table->integer('expire_month');            
            $table->integer('expire_year');
            $table->string('status')->nullable()->default('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_card_detail');
    }
}
