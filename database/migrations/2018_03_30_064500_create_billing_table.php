<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->nullable()->default(0);
            $table->integer('user_id')->default(0)->nullable();

            $table->integer('unit')->nullable()->default(0)->comment("Unit Total");

            $table->string('buyer_name')->nullable()->default(null);
            $table->string('buyer_contact')->nullable()->default(null);
            $table->string('buyer_email')->nullable()->default(null);
            


            $table->float('item_price')->nullable()->default(0);
            $table->enum('price_currency', ['USD'])->default('USD');
            $table->integer('item_qty')->nullable()->default(0);
            $table->float('discount')->nullable()->default(0);
            

            
            $table->enum('billing_type', ['paypal','stripe','trial'])->default('stripe');
            $table->enum('billing_status', ['pending','active','expired','suspended'])->default('pending');
           

            $table->string('customer_stripe_id')->nullable();
            $table->string('subscription_id')->nullable()->default(null);
            $table->string('plan_id')->nullable()->default(null);

            $table->longText('subscription_object')->nullable()->default(null);
            $table->longText('package')->nullable()->default(null);

            $table->dateTime('cycle_start_date')->nullable()->default(null);
            $table->dateTime('last_billing_date')->nullable()->default(null);
            $table->dateTime('next_billing_date')->nullable()->default(null);
            $table->dateTime('expiry_date')->nullable()->default(null);
			
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing');
    }
}
