<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempratureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temprature', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('location_city')->nullable()->default(null);
            $table->string('location_postalcode')->nullable()->default(null);
            $table->string('location_regions')->nullable()->default(null);
            $table->string('location_country')->nullable()->default(null);
            $table->string('location_countrycode')->nullable()->default(null);
            
            $table->string('location_longitude')->nullable()->default(null);
            $table->string('location_latitude')->nullable()->default(null);
            
            
            $table->float('temp')->nullable()->default(null);
            $table->float('humidity')->nullable()->default(null);
            $table->float('pressure')->nullable()->default(null);
            $table->float('uv')->nullable()->default(null);
            $table->string('rain')->nullable()->default(null);
            $table->string('snow')->nullable()->default(null);
            $table->string('sunny')->nullable()->default(null);
            $table->string('cloud')->nullable()->default(null);
            $table->string('wind')->nullable()->default(null);
            
            
            $table->date('weather_date')->nullable()->default(null);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temprature');
    }
}
