<header>
    <div class="header-div">

        <div class="header-top-div clearfix">
                <div class="logo-div clearfix">
                        <a href="{{url('')}}"><img src="{!! asset('/frontend/images/logo.png') !!}" alt="logo" class="img-responsive" /></a>
                    </div><!-- end of logo-div -->
                  
                    <div class="nav-div clearfix">
                            
                        <nav class="navbar clearfix">
                            
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header clearfix">
                                <button type="button" class="navbar-toggle collapsed clearfix" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                                    
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse clearfix" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav clearfix">
                                    <li class="{{ request()->is('learn-more') ? 'active' : '' }}" ><a href="{{url('learn-more')}}">Learn More</a></li>
                                    <li class="{{ request()->is('plan') ? 'active' : '' }}"><a href="{{url('plan')}}">Prices</a></li>
                                    <li class="{{ request()->is('support') ? 'active' : '' }}"><a href="{{url('support')}}">Support</a></li>
                                    @if (Auth::check())
                                    <li class="{{ request()->is('social-sync') ? 'active' : '' }}"><a href="{{url('social-sync')}}">My Campaigns</a></li>
                                    
                                    <li class="desktop-hidden "> <a href="#" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" > Logout   </a> </li>
                                    @else
                                        <li class="desktop-hidden "><a href="{{url('register')}}">Sign Up</a></li>
									    <li class="desktop-hidden "><a href="{{url('login')}}">Log In</a></li>
                                    @endif
									
                                </ul>
                            </div><!-- /.navbar-collapse -->
                            
                            
                        </nav>
                    </div><!-- end of nav-div -->

                    <div class="top-strip-div clearfix mobile-hidden">
                        @if (Auth::check())
                            <span><a href="#" class="singup-link" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Logout</a></span>
                            
                        @else
                            <span><a href="{{url('register')}}" class="singup-link">Sign Up</a></span>
                            <span><a href="{{url('login')}}" class="login-link">Log In</a></span>
                        @endif
                    </div><!-- end of top strip -->
                        

        </div>



    </div>
    @if (Auth::check())
       <form id="logout-form" action="{{ url('/logout') }}" method="POST"style="display: none;">
                       {{ csrf_field() }}
        </form>
     @endif
</header>