

<section class="footer-container clearfix">
    <div class="container">
        <div class="row">
            <div class="footer-div clearfix">
                <div class="col-20">
                <a href="{{url('/')}}"><img src="{!! asset('/frontend/images/footer-logo.png') !!}" alt="weather footer logo" class="img-responsive m-center"></a>
                </div>
                <div class="col-20">
                    <ul>
                        <li><h3>Product</h3></li>
                        @if (!Auth::check())
                        <li><a href="{{url('register')}}">Sign Up</a></li>
                        @endif
                        
                        <li><a href="{{url('learn-more')}}">Learn More</a></li>
                        <li><a href="{{url('support')}}">FAQ's</a></li>
                        <li><a href="{{url('plan')}}">Prices</a></li>
                    </ul>
                </div>
                <div class="col-20">
                    <ul>
                        <li><h3>Company</h3></li>
                        <li><a href="{{url('support')}}">Support</a></li>
                    </ul>
                </div>
                <div class="col-20">
                    <ul>
                        <li><h3>Legal</h3></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                    </ul>
              </div>
                <div class="col-20">
                    <div class="icon-div">
                        <span class="top-social-span">
                            <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="#" target="_blank"><i class="fab fa-twitter"></i></a>
                        </span>
                    </div>
                    <p><a href="#">
                            <?php $today = getdate(); ?>
                            Copyright &copy; {{$today['year']}} {{ config('app.name') }}
                    </p>
                </div>


            </div><!-- end of footer-div -->
        </div>
    </div>
</section>   