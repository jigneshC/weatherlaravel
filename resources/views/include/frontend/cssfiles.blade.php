<link href="{!! asset('/frontend/css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />
<link href="{!! asset('/frontend/css/bootstrap.min.css') !!}" media="all" rel="stylesheet" type="text/css" />

<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

<link href="{!! asset('/frontend/css/custome.css') !!}" media="all" rel="stylesheet" type="text/css" />
