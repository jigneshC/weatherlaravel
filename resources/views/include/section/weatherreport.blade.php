<section class="weather-report-container clearfix">
    <div class="container">
        <div class="row">
            <div class="weather-report-div clearfix">
                <div class="w-r-blk-1">
                    <h3>21<sup>0</sup></h3>
                    <h4>New York</h4>
                    <div class="hl-div">
                        <span>H 75<sup>0</sup></span>
                        <span>L 65<sup>0</sup></span>
                    </div>
                </div>
                <div class="w-r-blk-2">                    
                    <img src="{!! asset('/frontend/images/rainfall.png') !!}" alt="Rainfall" class="img-responsive"><!-- end of images report img -->
                </div>
                <div class="w-r-blk-3">
                    <div class="txt-blk">
                        <h3>Rain Showers</h3>
                        <h4>Monday 25th June</h4>
                    </div>
                
                </div>
            </div><!-- end of weather-report-div -->
        </div>
    </div>
</section><!-- end of weather-report-container -->