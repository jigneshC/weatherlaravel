<link rel="stylesheet" href="{!! asset('/assets/stylesheets/plugins/fuelux/wizard.css') !!}">
<!-- / bootstrap [required] -->
<link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all" rel="stylesheet"  type="text/css"/>


<link href="{!! asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<!-- / theme file [required] -->
<link href="{!! asset('assets/stylesheets/light-theme.css') !!}" media="all" id="color-settings-body-color"
          rel="stylesheet" type="text/css"/>
<!-- / coloring file [optional] (if you are going to use custom contrast color) -->
<link href="{!! asset('assets/stylesheets/theme-colors.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<!-- / demo file [not required!] -->
<link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link href="{!! asset('assets/stylesheets/design.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link href="{!! asset('assets/stylesheets/style.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link href="{!! asset('assets/stylesheets/ges-fonts.css?family=Montserrat') !!}" media="all" rel="stylesheet" type="text/css"/>    
<link rel="stylesheet" href="{!! asset('assets/build/css/intlTelInput.css')!!}">

<!-- Data Table -->
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />