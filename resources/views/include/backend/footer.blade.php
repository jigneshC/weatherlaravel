<footer id='footer'>
    <div class='footer-wrapper'>
        <div class='row'>
            <div class='col-sm-6 text'>
                <?php $today = getdate(); ?>
                Copyright &copy; {{$today['year']}} {{ config('app.name') }}
            </div>
            <div class='col-sm-6 buttons'>
                
            </div>
        </div>
    </div>
</footer>