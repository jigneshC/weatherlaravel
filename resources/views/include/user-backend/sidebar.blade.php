<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->

                    <li class="{{ request()->is('Dashboard') ? 'active' : '' }}" ><a href="{{url('Dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    <li class="{{ request()->is('fb-compaigns') ? 'active' : '' }}">
                        <a href="#"><i class="fa fa-rocket sidebar-nav-icon"></i><span>My Campaigns</span></a>
                        <ul>
                            <li><a href="{{url('fb-compaigns')}}"><i class="fab fa-facebook-f sidebar-nav-icon"></i> Facebook</a></li>
                        </ul>
                    </li>

                    <li class="{{ request()->is('Account/*') ? 'active' : '' }}" ><a href="{{url('profile')}}"><i class="fa fa-cogs sidebar-nav-icon"></i><span> Account Details</span></a></li>
                    <li class="{{ request()->is('Billing/*') ? 'active' : '' }}"><a href="{{url('Billing/subscription')}}"><i class="fa fa-credit-card sidebar-nav-icon"></i><span> Billing</span></a></li>
                    <!-- /main -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->