<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html"><img src="{!! asset('/frontend/images/logo.png') !!}" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>


            </li>
        </ul>

        @if (Session::has('session_notification_header2') && Session::get('session_notification_header2')!="")
        <p class="navbar-text"><span class="label bg-success">{{ Session::get('session_notification_header2') }}</span></p>
        @endif
        

        <ul class="nav navbar-nav navbar-right">
            


            

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{!! asset('user-backend/assets/images/placeholder.jpg') !!}" alt="">
                    <span>{{Auth::user()->name}}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{url('/profile')}}"><i class="icon-user-plus"></i> My profile</a></li>
                    <li><a href="{{url('/profile/change-password')}}"><i class="icon-user-plus"></i> Change Password</a></li>
                    <li><a href="#"><i class="icon-coins"></i> My balance</a></li>

                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="#" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" ><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->

@if (Auth::check())
<form id="logout-form" action="{{ url('/logout') }}" method="POST"style="display: none;">
    {{ csrf_field() }}
</form>
@endif
