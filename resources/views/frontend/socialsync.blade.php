@extends('layouts.frontend')
@section('title','Social Sync - WeatherAdvert')
@section('body_class','general-area-bg-white')

@section('content')
<div class="main-content-area-all clearfix">
    <div class="container">
        <div class="row">
            <section class="add-area clearfix">
                <div class="container">
                    <div class="row">
                        <div class="add-card add-cardboard clearfix">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="cancel-icon">
                                    <div class="alert @if($fb_sync_status) alert-success @else alert-danger @endif alert-dismissible fade in">

                                        @if($fb_sync_status)
                                        Synced
                                        @else
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        Not synced
                                        @endif
                                    </div>
                                </div><!-- end of cancel alert -->
                                <div class="add-card add-cardboard clearfix">
                                    <div class="facebook-ads">
                                        <a href="{{$login_url}}" id="fb_login_noww"><img src="{!! asset('/frontend/images/facebook-ads.png') !!}" alt="facebook ads" class="img-responsive"></a>
                                        <div class="loader-img hide">
                                            <img src="{!! asset('/user-backend/assets/images/loading_icon.gif') !!}" height="150" alt="facebook ads" class="img-responsive padding-0">
                                        </div>
                                    </div>    
                                </div> <!-- end of images odf facebook png -->
                            </div>
                        </div><!-- end ads div -->
                    </div>    
                </div>
            </section><!-- end of ads-area -->
        </div><!-- end of row -->
    </div><!-- end of container -->
</div><!-- end of main content area all -->

@endsection

//Facebook login js
@push('js')
<script>
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            testAPI(response.authResponse.accessToken,response.authResponse.expiresIn);
            
        } else {
            //  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
        }
    }
    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: "{{ \config('syncaccount.fb.FACEBOOK_APP_ID')}}",
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.12' // use graph api version 2.8
        });
        /* FB.getLoginStatus(function (response) {
         statusChangeCallback(response);
         });*/

    };
    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI(accessToken,expiresIn) {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function (response) {
            // document.getElementById('status').innerHTML = response.name;
            $('.fb_login').hide();
            $('.fb_logout').show();
            
            var data = {
                'id': response.id,
                'name': response.name,
                'accesstoken': accessToken,
                'expiresIn':expiresIn,
                'accesstokenLongLive': null,
                'expiresInLongLive':0,
            };

            FB.api('oauth/access_token', {
                client_id: "{{ \config('syncaccount.fb.FACEBOOK_APP_ID')}}", // FB_APP_ID
                client_secret: "", // FB_APP_SECRET
                grant_type: 'fb_exchange_token',
                fb_exchange_token: accessToken // USER_TOKEN
            }, function (res) {
                console.log(res);
                if(!res || res.error) {
                    console.log(!res ? 'error occurred' : res.error);
                }else{
                    var accessTokenl = res.access_token;
                    if(typeof accessTokenl != 'undefined'){
                        data.accesstokenLongLive=accessTokenl;
                        data.expiresInLongLive=res.expires_in;
                    }
                }
                $.ajax({
                    url: "{{url('savedata')}}",
                    method: 'POST',
                    data: data,
                    headers: {
                        "X-CSRF-TOKEN": "{{ csrf_token() }}"
                    },
                    beforeSend: function (xhr) {
                        $('.facebook-ads').find(".loader-img").removeClass('hide');
                    },
                    success: function (data) {
                        $('.facebook-ads').find(".loader-img").addClass('hide');
                        location.href = "{{url('Billing/subscription')}}";
                    },
                    error: function (xhr, status, error) {
                        $('.facebook-ads').find(".loader-img").addClass('hide');
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!', erro)
                    }
                });
            });
            
            


        });
    }
    function logoutAPI() {
        FB.logout(function (response) {
            $('.fb_login').show();
            $('.fb_logout').hide();
            //  document.getElementById('status').innerHTML = 'Please log ' +'into this app.';
        });
    }
    var fbLogin = function () {

    };

    $(function () {
        $(document).on('click', '#fb_login_now', function (e) {
            FB.login(
                    function (response) {
                        statusChangeCallback(response);
                    },
                    {
                        scope: 'public_profile,email,ads_management,manage_pages,business_management'
                    }
            );
        });
    });


</script>

@endpush