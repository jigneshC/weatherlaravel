@extends('layouts.frontend')
@section('title','Plan - WeatherAdvert')
@section('body_class','general-bg')

@section('content')
<div class="main-content-area-all clearfix">
        <div class="container">
            <div class="row">
                <section class="plan-more-container clearfix">
                    <div class="plan-more-div-1 clearfix">
                        <div class="col-md-12 col-sm-12">    
                            <h3>Find your perfect plan</h3>
                            @if (!Auth::check())
                            <span><a href="#" class="trial-link">Try our Free Trial now &gt;</a></span>
                            <span class="trial-txt-1">Free {{ \config('settings.FREE_TRIAL_DAY')}} day trial, no card details required for sign up.</span>
                            @endif
                        </div><!-- end of plan-more-div -->
                        
                        <div class="plan-grid-div">
                            <div class="four-columns group">
                                @foreach($packages as $package)
                                    <div class="col-md-4 col-sm-4 padd-card-20">  
                                        <div class="plan-card-div-1">
                                            <div class="img-icecream">
                                                <img src="{{$package->image}}" alt="ice-cream" style="height: 200px; width: 245px; margin-bottom: 25px;">
                                            </div>
                                        </div>
                                        <div class="plan-card-div-2">
                                            <a href="#" class="btn-ice-title">{{$package->package_class}}</a>
                                        </div>
                                        <div class="plan-card-div-3 {{$package->class}}">
                                                <h3>{{$package->name}}</h3>
                                                <h3>{{$package->currency.''.$package->price}}</h3>
                                                <span>{{$package->validity.' '.ucfirst($package->validity_type)}}</span>
                                                <ul type="square">
                                                    @for ($i = 0; $i < count($package->details); $i++)
                                                        <li>{{$package->details[$i]}}</li>
                                                    @endfor
                                                </ul>
                                            </div>
                                        </div><!-- col-1 -->                                        
                                @endforeach
                            </div><!-- four columns  -->
                        </div><!-- end of plan-grid-div -->
    
    
                    </div><!-- end of plan-more-div-1 -->    
                </section><!-- end of plan-more-container -->  
            </div><!-- end of row -->
        </div><!-- end of container -->
    </div><!-- end of main content area all -->
@endsection
