@extends('layouts.frontend')
@section('title','Home')
@section('body_class','')

@section('content')
     @include('include.section.banner')
	 @include('include.section.howitwork')
	 @include('include.section.weatherfilter')
	 @include('include.section.weatherreport')
	 @include('include.section.weatheraction')
@endsection
