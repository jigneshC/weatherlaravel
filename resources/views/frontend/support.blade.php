@extends('layouts.frontend')
@section('title','Support')
@section('body_class','general-bg')

@section('content')
<div class="main-content-area-all clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <section class="learn-more-container clearfix">
                        <div class="learn-more-div-1 clearfix">
                        <!--    <div class="iphone-img"><img src="images/iphone.png" alt="" class="img-responsive"></div> -->
                            <h3>Learn More</h3>
                            <div class="txt-div clearfix">
                                <p class="more-txt-1">Weather triggered advertising for Facebook and Instagram.  </p>
                                <p class="more-txt-2">Is your business weather dependant?</p>
                                <p class="more-txt-3">Are you paying for Facebook adverts during weather conditions that don&apos;t benefit your business?</p>
                                <p class="more-txt-4">For Example, running an advert to sell Ice Cream at the beach, when it&apos;s raining.</p>
                            </div>
                        </div><!-- end of learn-more-div -->
                    </section><!-- end of learn-more-container -->  
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <section class="learn-more-container clearfix">
                        <div class="learn-more-div-2 clearfix">
                            <div class="iphone-img-1"><img src="{!! asset('/frontend/images/iphone.png') !!}" alt="iphone" class="img-responsive"></div>
                            
                            <div class="txt-div-1 clearfix">
                                <p class="learn-more-txt-1">We&apos;ve created a unique tool that will automatically switch on or off your Facebook and Instagram adverts depending upon the weather conditions.</p>
                                <p class="learn-more-txt-1">Our tool will prevent you adverting in locations where the weather has a negative effect on your business, saving you money and boosting sales in territories that will benefit your business.  </p>
                            </div>
                        </div><!-- end of learn-more-div -->
                    </section><!-- end of learn-more-container -->  
                </div>
            </div><!-- end of row -->
            <!-- second row -->          
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <section class="learn-more-container-2 clearfix">
                        <div class="learn-more-div-21 clearfix">
                            <div class="txt-div-1 clearfix">
                                <p class="learn-more-txt-1">From as little as <span class="special-text">&pound;9.99</span> per month our tool will integrate with your social media advertising campaigns to automatically 
                                        activate your adverts based on the weather conditions. This would allow your adverts to be switched on or off if the 
                                        weather changes, saving you from advertising during conditions that would not benefit your business or service. 
                                        This can be scheduled across different territories, states, zip/postal codes and countries at the same time. 
                                </p>
                                <p class="learn-more-txt-1">Adverts across several different locations and postal codes will be automatically switched on and off as the weather 
                                        changes, allowing you to react automatically and getting your advert to the most relevant customers.
                                </p>
                                <p class="learn-more-txt-1">
                                        Our premium package includes our unique AI integration software, giving you recommendations and detailed feedback on your active campaigns to further enhance targeting and build new audiences. 
                                </p>
                                <p class="learn-more-txt-2">
                                        Our tool has been designed to help and save you money, adverting to customers during the most 
                                        relevant weather conditions, so why not start a free {{ \config('settings.FREE_TRIAL_DAY')}}-day trial!
                                </p>
                                @if (!Auth::check()) <span><a href="#" class="trial-link">Try our Free Trial now &gt;</a></span> @endif
                            </div>
                        </div><!-- end of learn-more-div -->
                    </section><!-- end of learn-more-container -->  
                </div>
            </div><!-- end of second row -->    
                
            
        </div><!-- end of container -->
    </div><!-- end of main content area all -->
    @endsection
