@extends('layouts.admin')

@section('title','Facebook')
@section('pageTitle','Facebook')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Users
                    </div>
                    <div class="actions">
                        
                    </div>

                </div>
                <div class="box-content panel-body">



                    <div class="row">
                        <div class="col-md-6">
                            {{--  @if(Auth::user()->can('access.user.create'))
                                <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm"
                                   title="Add New User">
                                    <i class="fa fa-plus" aria-hidden="true"></i>Add New
                                </a>

                            @endif  --}}
                        </div>

                        <div class="col-md-3">

                        </div>

                        

                    </div>
</div>
<div class="box-content panel-body">



                    <div class="row">

                    <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($fbusers as $user)
                                <tr>
                                    <td> {{$user->id}}</td>
                                    <td> <a href="https://www.facebook.com/ads/manager/account/campaigns/" target="_blank">{{$user->name}}</a></td>                                    
                                </tr>   
                                @endforeach
                            </tbody>

                        </table>
                    </div>
</div>
                </div>
            </div>
        </div>
    </div>
@endsection




