

@extends('layouts.admin')

@section('title','Facebook Campaigns')
@section('pageTitle','Facebook Campaigns')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Campaigns
                    </div>
                    <div class="actions">
                        
                    </div>

                </div>
                <div class="box-content panel-body">



                    <div class="row">
                        <div class="col-md-6">
                           
                        </div>

                        <div class="col-md-3">

                        </div>

                        

                    </div>
</div>
<div class="box-content panel-body">



                    <div class="row">

                    <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                {{--  <th>Id</th>  --}}
                                {{--  <th>Campaigns</th>  --}}
                                
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($campaigns as $campaign)
                                <tr>
                                    {{--  <td> {{$user->campaign_id}}</td>  --}}
                                    <td> {{$campaign->name}}  
                                       @if(count($campaign->adsets)>0)
                                        </br> </br>                                        
                                        <i class="fa fa-check"></i> Adsets
                                        <ul>                                           
                                            @foreach($campaign->adsets as $adset)
                                            <li>{{$adset->name}}</li>  
                                            @endforeach                                         
                                        </ul> 
                                        @endif  
                                    </td>                              
                                </tr>   
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




