@extends('layouts.user-backend')

@section('title','Dashboard')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Dashboard</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">


            <div class="col-md-12">
                <div class="panel panel-flat">
                   

                    <div class="panel-body">
                        <div class="tabbable">
                            <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                                <li class="{{ request()->is('Billing/subscription') ? 'active' : '' }}" ><a href="{{url('Billing/subscription')}}" >Current Subscription</a></li>
                                <li class="{{ request()->is('Billing/plans') ? 'active' : '' }}"><a href="{{url('Billing/plans')}}" >Change Plan</a></li>
                                <li class="{{ request()->is('Billing/unit-history') ? 'active' : '' }}"><a href="{{url('Billing/unit-history')}}" >Unit History</a></li>
                                <li class="{{ request()->is('Billing/history') ? 'active' : '' }}"><a href="{{url('Billing/history')}}" >Subscription History</a></li>
                                
                            </ul>

                           
                                

                               
                                    @foreach($packages->chunk(4) as $chunk) 
                                    <div class="row pricing-tables">
                                        @foreach($chunk as $item)
                                        <div class="col-sm-6 col-md-3">
                                            <div class="pricing-table">
                                                <div class="header">{{$item->package_class}}</div>
                                                <div class="price @if(isset($package_cls[$item->package_class])) {{$package_cls[$item->package_class]}} @endif">
                                                    <span>@if($item->price_currency=="USD") $ @endif {{$item->price}}</span>
                                                </div>
                                                <ul class="list-unstyled features">

                                                    <li>
                                                        <strong>{{$item->validity}} {{$item->validity_type}} </strong>
                                                        Validity
                                                    </li>
                                                    <li>
                                                        <strong>{{$item->name}}</strong>
                                                    </li>
                                                    <li>
                                                        <strong>{{$item->desc}}</strong>

                                                    </li>
                                                </ul>
                                                <div class="footer">
                                                    <a class="btn btn-primary" href="{{url('confirm-order/'.$item->id)}}"><i class="icon-signin"></i>  Order Now </a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="group-header">

                                    </div>
                                    @endforeach  
                                
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


