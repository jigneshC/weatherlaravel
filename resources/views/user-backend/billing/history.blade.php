@extends('layouts.user-backend')

@section('title','Subscription')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Billing / Subscription</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">


            <div class="col-md-12">
                <div class="panel panel-flat">


                    <div class="panel-body">
                        <div class="tabbable">
                           <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                                <li class="{{ request()->is('Billing/subscription') ? 'active' : '' }}" ><a href="{{url('Billing/subscription')}}" >Current Subscription</a></li>
                                <li class="{{ request()->is('Billing/plans') ? 'active' : '' }}"><a href="{{url('Billing/plans')}}" >Change Plan</a></li>
                                <li class="{{ request()->is('Billing/unit-history') ? 'active' : '' }}"><a href="{{url('Billing/unit-history')}}" >Unit History</a></li>
                                <li class="{{ request()->is('Billing/history') ? 'active' : '' }}"><a href="{{url('Billing/history')}}" >Subscription History</a></li>
                                
                            </ul>

                            <div class="tab-content">

                                <div class="panel invoice-grid">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h6 class="text-semibold no-margin-top">Leonardo Fellini</h6>
                                                <ul class="list list-unstyled">
                                                    <li>Invoice #: &nbsp;0028</li>
                                                    <li>Issued on: <span class="text-semibold">2015/01/25</span></li>
                                                </ul>
                                            </div>

                                            <div class="col-sm-6">
                                                <h6 class="text-semibold text-right no-margin-top">$8,750</h6>
                                                <ul class="list list-unstyled text-right">
                                                    <li>Method: <span class="text-semibold">SWIFT</span></li>
                                                    <li class="dropdown">
                                                        Status: &nbsp;
                                                        <a href="#" class="label bg-danger-400 dropdown-toggle" data-toggle="dropdown">Overdue <span class="caret"></span></a>
                                                        <ul class="dropdown-menu dropdown-menu-right active">
                                                            <li class="active"><a href="#"><i class="icon-alert"></i> Overdue</a></li>
                                                            <li><a href="#"><i class="icon-alarm"></i> Pending</a></li>
                                                            <li><a href="#"><i class="icon-checkmark3"></i> Paid</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#"><i class="icon-spinner2 spinner"></i> On hold</a></li>
                                                            <li><a href="#"><i class="icon-cross2"></i> Canceled</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <ul>
                                            <li><span class="status-mark border-danger position-left"></span> Due: <span class="text-semibold">2015/02/25</span></li>
                                        </ul>

                                        <ul class="pull-right">
                                            <li><a href="#" data-toggle="modal" data-target="#invoice"><i class="icon-eye8"></i></a></li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-printer"></i> Print invoice</a></li>
                                                    <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-file-plus"></i> Edit invoice</a></li>
                                                    <li><a href="#"><i class="icon-cross2"></i> Remove invoice</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


