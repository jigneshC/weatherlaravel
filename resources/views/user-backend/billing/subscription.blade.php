@extends('layouts.user-backend')

@section('title','Subscription')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Billing / Subscription</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">


            <div class="col-md-12">
                <div class="panel panel-flat">


                    <div class="panel-body">
                        <div class="tabbable">
                            <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                                <li class="{{ request()->is('Billing/subscription') ? 'active' : '' }}" ><a href="{{url('Billing/subscription')}}" >Current Subscription</a></li>
                                <li class="{{ request()->is('Billing/plans') ? 'active' : '' }}"><a href="{{url('Billing/plans')}}" >Change Plan</a></li>
                                <li class="{{ request()->is('Billing/unit-history') ? 'active' : '' }}"><a href="{{url('Billing/unit-history')}}" >Unit History</a></li>
                                <li class="{{ request()->is('Billing/history') ? 'active' : '' }}"><a href="{{url('Billing/history')}}" >Subscription History</a></li>

                            </ul>

                            <div class="tab-content">

                                @if($current_subscription)
                                @php($package = json_decode($current_subscription->package))
                                <div class="panel invoice-grid">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h6 class="text-semibold no-margin-top">{{$package->name}}</h6>
                                                <ul class="list list-unstyled">
                                                    <li>Id #: &nbsp;00{{$current_subscription->id}}</li>
                                                    <li>Issued on: <span class="text-semibold">{{$current_subscription->cycle_start_date}}</span></li>
                                                </ul>
                                            </div>

                                            <div class="col-sm-6">
                                                <h6 class="text-semibold text-right no-margin-top">{{ $current_subscription->unit }} Compagnie </h6>
                                                <ul class="list list-unstyled text-right">
                                                    <li>Plan: <span class="text-semibold">{{ $current_subscription->billing_type }}</span></li>
                                                    <li class="dropdown">
                                                        Status: &nbsp;
                                                        <a href="#" class="label bg-danger-400 dropdown-toggle" data-toggle="dropdown">{{$current_subscription->billing_status}} </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <ul>
                                            <li><span class="status-mark border-danger position-left"></span> Due: <span class="text-semibold">{{$current_subscription->expiry_date}}</span></li>
                                        </ul>

                                        
                                    </div>
                                </div>
                                @elseif($count_subscription <= 0 )
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-body border-top-primary text-center">
                                            <form class="form-horizontal" method="POST" action="{{ url('Billing/subscription/trial') }}">
                                                    {{ csrf_field() }}
                                            <h6 class="no-margin text-semibold">{{ \config('settings.free_trial_pack_info.name')}}</h6>
                                            <p class="text-muted content-group-sm">{{ \config('settings.free_trial_pack_info.desc')}}</p>
                                            <p class="text-muted content-group-sm">{{ \config('settings.free_trial_pack_info.unit')}} Compagnie / {{ \config('settings.free_trial_pack_info.FREE_TRIAL_DAY')}} days</p>
                                           

                                            <button type="submit" class="btn btn-primary">Start Your Free Trial Now !</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @endif

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


