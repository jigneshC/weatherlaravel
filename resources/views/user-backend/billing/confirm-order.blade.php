@extends('layouts.user-backend')

@section('title','Confirm Order')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Confirm Order</h5>
        <div class="heading-elements">
            
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => '/confirm-order', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}
                {!! Form::hidden('package_id',$package->id, ['class' => 'form-control','id'=>'package_id']) !!}
                {!! Form::hidden('step',1, ['class' => 'form-control','id'=>'step']) !!}
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-group">
                                        {!! Form::label('buyer_name',"Buyer Name", ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::text('buyer_name',Auth::user()->name, ['class' => 'form-control']) !!}
                                            {!! $errors->first('buyer_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('buyer_email',"Buyer Email", ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::text('buyer_email',Auth::user()->email, ['class' => 'form-control']) !!}
                                            {!! $errors->first('buyer_email', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        {!! Form::label('buyer_contact',"Buyer Contact", ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::text('buyer_contact',null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('buyer_contact', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    

                                    

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Next <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


