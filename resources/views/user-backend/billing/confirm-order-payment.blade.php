@extends('layouts.user-backend')

@section('title','Confirm Billing & Payment')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Confirm Billing & Payment</h5>
        <div class="heading-elements">
            
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => '/order/payment', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}
                {!! Form::hidden('billing_id',$billing->id, ['class' => 'form-control','id'=>'billing_id']) !!}
                {!! Form::hidden('step',2, ['class' => 'form-control','id'=>'step']) !!}
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-group">
                                        {!! Form::label('card_no',"Card No", ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::number('card_no',null, ['class' => 'form-control','min'=>100]) !!}
                                            {!! $errors->first('card_no', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('ccExpiryMonth',"Expiry Month", ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::number('ccExpiryMonth',null, ['class' => 'form-control','min'=>1]) !!}
                                            {!! $errors->first('ccExpiryMonth', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        {!! Form::label('ccExpiryYear',"Expiry Year", ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::number('ccExpiryYear',null, ['class' => 'form-control','min'=>100]) !!}
                                            {!! $errors->first('ccExpiryYear', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        {!! Form::label('cvvNumber',"CVV No.", ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::number('cvvNumber',null, ['class' => 'form-control','min'=>100]) !!}
                                            {!! $errors->first('cvvNumber', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    

                                    

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Start Subscription <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


