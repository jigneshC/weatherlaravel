<!-- Iconified modal -->
<div id="modal_adsets" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title"> &nbsp; Feasible Weather Setting </h5>
            </div>

            {!! Form::open(['url' => '','id'=>'adset_form','class' => 'form-horizontal adset_form', 'files' => true]) !!}
            
            <div class="modal-body">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                       
                    <div class="panel-body">
                        
                        {!! Form::hidden('fbad_id',0, ['class' => 'form-control','id'=>'fbad_id']) !!}
                        {!! Form::hidden('adset_ids',"", ['class' => 'form-control','id'=>'adset_ids']) !!}
                        
                        {!! Form::hidden('default_latitude',\config('settings.default_latitude'), ['class' => 'form-control','id'=>'default_latitude']) !!}
                        {!! Form::hidden('default_longitude',\config('settings.default_longitude'), ['class' => 'form-control','id'=>'default_longitude']) !!}
                        <div class="form-group">
                            <label>Location:</label>
                            <input type="text" name="location_name" class="form-control location_name" id="location_name">
                            <div id="map-canvas" class="gmaps" style="height: 300px;width: 100%"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Latitude:</label>
                                    <input type="text"  name="location_latitude" class="form-control location_latitude" id="location_latitude">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Longitude:</label>
                                    <input type="text"  name="location_longitude" class="form-control location_longitude" id="location_longitude">
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Country:</label>
                                    <input type="text"  name="location_countrycode" class="form-control location_countrycode" id="location_countrycode">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>State:</label>
                                    <input type="text"  name="location_regions" class="form-control location_regions" id="location_regions">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>City:</label>
                                    <input type="text"  name="location_city" class="form-control location_city" id="location_city">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Zip:</label>
                                    <input type="text"  name="location_postalcode" class="form-control location_postalcode" id="location_postalcode">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                <button type="submit" class="btn btn-primary"><i class="icon-check"></i> Save</button>
            </div>
        </div>
            {!! Form::close() !!}
    </div>
</div>



@push('js')

<script src="{!! asset('assets/js/google-search-location-map.js')!!}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{\config('settings.GOOGLE_MAP_API')}}&libraries=places&callback=initAutocomplete" async defer> </script>

<script>
    $(document).ready(function () {

        /***************action model******************/
        var _model = "#modal_adsets";
        var reload = "0";
        $(document).on('click', '.modal_adsets_open', function (e) {
            reload = $(this).attr('reload');  
            $("#adset_ids").val("");
            $("#fbad_id").val(0);
            var id=$(this).attr('data-id');   
            if(id==0 || id=="0"){
                var adset_ids = "";
                var cnt = 0;
                $('.adset_ids:checked').each(function () {
                    if(cnt==0){
                        adset_ids = $(this).val();
                        id = $(this).val();
                    }else{
                        adset_ids = adset_ids+","+$(this).val();
                    }
                    cnt++;
                });
                if(adset_ids==""){
                    return false;
                }else{
                    $("#adset_ids").val(adset_ids);
                }
                
            }
            $("#fbad_id").val(id);
                var url = "{{url('fb-adsets')}}/"+id+"/edit";   
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (result) {
                        var data=result.data;
                         $.each(data, function(key, value){
                            $("#"+key).val(value);
                         });
                         mapRefresh();
                         $(_model).modal('show');
                    }
               }); 
           
            
            
            return false;
        });

        $('.adset_form').submit(function(event) {
          
            var error_msg = "";

            if(error_msg!=""){
                $(".form_submit_error").html(error_msg).show().delay(5000).fadeOut();
                return false;
            }
            var id = $(".adset_form #fbad_id").val();
            var url = "{{url('fb-adsets')}}/"+id;
            var method = "POST";
            var formData = $("#adset_form").serialize();
            
            $.ajax({
                type: method,
                url: url,
                data: formData,
                success: function (data) {
                    $(_model).modal('hide');
                    toastr.success('Action Success!', data.message);
                    if(reload=="1"){
                        location.reload();
                    }else{
                        datatable.fnDraw(false);
                    }
                    
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });

            return false;
        });

        
    });

</script>
@endpush