@extends('layouts.user-backend')

@section('title','Campaigns Adsets')
@section('content')


<div class="panel panel-flat">
    <div class="panel-heading">


    </div>

    <div class="panel-body" >
        <div class="tabbable">
            <ul class="nav nav-tabs bg-slate nav-tabs-component nav-justified">
                <li class="{{ request()->is('subscription') ? 'active' : '' }}" >
                    <a href="#" class="filter_type" data-type="temparature" >Temp</a>
                </li>
                <li class=""><a href="#" class="filter_type" data-type="rain">Rain</a></li>
                <li class=""><a href="#" class="filter_type" data-type="snow">Snow</a></li>
                <li class=""><a href="#" class="filter_type" data-type="sunny">Sunny</a></li>
                <li class=""><a href="#" class="filter_type" data-type="cloud">Cloud</a></li>
                <li class=""><a href="#" class="filter_type" data-type="wind">Wind speed</a></li>
                <li class=""><a href="#" class="filter_type" data-type="humidity">Humidity</a></li>
                <li class=""><a href="#" class="filter_type" data-type="pressure">Pressure</a></li>
                <li class=""><a href="#" class="filter_type" data-type="uv">UV Index</a></li>



            </ul>
        </div>
    </div>

    {!! Form::model($adsetting, ['method' => 'post','url' => url('fb-ad-settings'),'class' => 'form-horizontal adset_form','id' => 'adset_form']) !!}
    
    {!! Form::hidden('ad_id',$fbad->id, ['class' => 'form-control','id'=>'fbad_id']) !!}
    <div class="panel-body"  style="display: none"  id="filter_container">
        @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
        <div class="row"  >
            <div class="col-md-4">
                <div class="panel panel-body border-top-danger">
                    <div class="text-center">
                        <h6 class="no-margin text-semibold">Time Frame</h6>

                    </div>

                    <div class="noui-slider-tooltip noui-slider-danger" id="day_range"></div>
                    <div class="clearfix">
                        {!! Form::hidden('min_day',"", ['class' => 'form-control min_day','id'=>'min_day']) !!}
                        {!! Form::hidden('max_day',"", ['class' => 'form-control max_day','id'=>'max_day']) !!}
                        <span class="val-demo pull-left">Value: <span id="min_day_val"></span></span>
                        <span class="val-demo pull-right">Value: <span id="max_day_val"></span></span>
                    </div>
                </div>


            </div>

            <div class="col-md-4">
                <div class="panel panel-body border-top-danger">
                    <div class="text-center">
                        <h6 class="no-margin text-semibold">Qualifier</h6>

                    </div>

                    <div class="noui-slider-tooltip noui-slider-danger" id="qualifier_container">

                    </div>

                </div>


            </div>

            <div class="col-md-4">
                <div class="panel panel-body border-top-danger">
                    <div class="text-center">
                        <h6 class="no-margin text-semibold">Amount</h6>
                    </div>

                    <div class="noui-slider-tooltip noui-slider-danger" id="amount_range"></div>
                    <div class="clearfix">
                        {!! Form::hidden('min_amount',"", ['class' => 'form-control min_amount','id'=>'min_amount']) !!}
                        {!! Form::hidden('max_amount',"", ['class' => 'form-control max_amount','id'=>'max_amount']) !!}
                        <span class="val-demo pull-left range_valueA">Value: <span id="min_amount_val"></span></span>
                        <span class="val-demo pull-right range_valueB">Value: <span id="max_amount_val"></span></span>
                    </div>
                </div>


            </div>
        </div>
                    <div class="row">
                        <!-- Bootstrap maxlength -->
                        <div class="panel panel-flat panel-collapsed" >
						<div class="panel-heading">
							<h5 class="panel-title">Advance Option</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		
			                	</ul>
		                	</div>
						</div>
                        <div class="panel-body" style="display: none">
							<div class="row">
								<div class="col-md-12">
									<div class="content-group-lg">
										<h6 class="text-semibold"><input type="checkbox" @if(isset($adsetting) && $adsetting->enable_frequency==1) checked @endif class="switchery " value = "1" name="enable_frequency" > FREQUENCY</h6>
										<div class="btn-group " data-toggle="buttons">
                                                                                    <label class="">
                                                                                        {!! Form::radio('frequency',1,null) !!} At least once
                                                                                    </label>

                                                                                    <label class="">
                                                                                        {!! Form::radio('frequency',2,null) !!}  At least once every day
                                                                                    </label>
                                                                                    <label class="">
                                                                                        {!! Form::radio('frequency',3,null) !!}  Continuously
                                                                                    </label>
                                                                                </div>
									</div>

									
								</div>

								
							</div>
                            <div class="row">
								<div class="col-md-12">
									<div class="content-group-lg">
										<h6 class="text-semibold"> <input type="checkbox" @if(isset($adsetting) && $adsetting->enable_hoursofday==1) checked @endif class="switchery " value = "1" name="enable_hoursofday" > HOURS OF THE DAY</h6>
										<div class="btn-group " data-toggle="buttons">
                                                                                    <label class="">
                                                                                        From {!! Form::select('hoursofday_from',\config('syncaccount.time24hrs'), null, ['class' => 'form-control input-sm']) !!}
                                                                                       
                                                                                    </label>

                                                                                    <label class="">
                                                                                        To {!! Form::select('hoursofday_to',\config('syncaccount.time24hrs'), null, ['class' => 'form-control input-sm']) !!}
                                                                                    </label>
                                                                                    
                                                                                </div>
									</div>

									
								</div>

								
							</div>
                            
                            
                                                    <div class="row">
								<div class="col-md-12">
									<div class="content-group-lg">
										<h6 class="text-semibold">  <input type="checkbox" @if(isset($adsetting) && $adsetting->enable_day_of_week==1) checked @endif class="switchery" value = "1" name="enable_day_of_week" > DAYS OF THE WEEK</h6>
                                                                               
										<div class="btn-group " data-toggle="buttons">
                                                                                    <?php
                                                                                    $day_of_week = [];
                                                                                    if(isset($adsetting)){
                                                                                        $day_of_week = json_decode($adsetting->day_of_week,true);
                                                                                    }
                                                                                    ?>
                                                                                    <input type="checkbox" name='day_of_week[]' value="0" @if($adsetting && ! in_array(0, $day_of_week)) @else checked="checked" @endif> Sun
                                                                                    <input type="checkbox" name='day_of_week[]' value="1" @if($adsetting && ! in_array(1, $day_of_week)) @else checked="checked" @endif> Mon
                                                                                    <input type="checkbox" name='day_of_week[]' value="2" @if($adsetting && ! in_array(2, $day_of_week)) @else checked="checked" @endif> Tue
                                                                                    <input type="checkbox" name='day_of_week[]' value="3" @if($adsetting && ! in_array(3, $day_of_week)) @else checked="checked" @endif> Wed
                                                                                    <input type="checkbox" name='day_of_week[]' value="4" @if($adsetting && ! in_array(4, $day_of_week)) @else checked="checked" @endif> Thu
                                                                                    <input type="checkbox" name='day_of_week[]' value="5" @if($adsetting && ! in_array(5, $day_of_week)) @else checked="checked" @endif> Fri
                                                                                    <input type="checkbox" name='day_of_week[]' value="6" @if($adsetting && ! in_array(6, $day_of_week)) @else checked="checked" @endif> Sat
                                                                                </div>
									</div>

									
								</div>

								
							</div>
						</div>
                                            </div>
					</div>
					<!-- /bootstrap maxlength -->
                    
                    
        <div class="row"  >
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="panel panel-body border-top-primary text-center">
                    <h6 class="no-margin text-semibold">If conditions are met:</h6>
                    <div class="btn-group " data-toggle="buttons">
                        <label class="btn btn-primary @if(isset($adsetting) && $adsetting->show_or_not==1) active @endif">
                            <i class="fa fa-eye"></i>{!! Form::radio('show_or_not',1,null,[]) !!} Show Ad
                        </label>

                        <label class="btn btn-primary @if(isset($adsetting) && $adsetting->show_or_not==0) active @endif">
                            <i class="fa fa-eye-slash"></i>{!! Form::radio('show_or_not',0,null,[]) !!} Don't show Ad
                        </label>
                    </div>




                </div>
                <div class="panel panel-body border-top-primary text-center">

                    {!! Form::hidden('weather_type',"", ['class' => '','id'=>'weather_type']) !!}
                    <div class="btn-group ">
                        <a href="{{url('fb-compaigns')}}" type="button" class="btn btn-primary btn bg-slate-700">Cancel</a>
                        <button type="submit" name="action" value="saveandpreview" class="btn btn-primary btn bg-slate-700">Save and Preview</button>
                        <button type="submit" name="action" value="apply" class="btn btn-primary btn bg-slate-700">Apply Change</button>
                    </div>



                </div>
            </div>

        </div>


    </div>
    {!! Form::close() !!}
</div>





@push('js')

<script>
    var wfilters = <?php echo json_encode(\config('syncaccount.wfilters')); ?>;
    var wday_range = <?php echo json_encode(\config('syncaccount.day_range')); ?>;

    var day_range_id = document.getElementById('day_range');
    var ammount_range_id = document.getElementById('amount_range');
    var qualifier_id = document.getElementById('qualifier_container');

    $(document).ready(function () {
        



        function setDayRangeView(day_range) {
            noUiSlider.create(day_range, wday_range);
            day_range.noUiSlider.on('update', function (values, handle) {
                if (handle == 0) {
                    $("#min_day_val").html(values[handle] + " d");
                    $("#min_day").val(values[handle]);
                } else {
                    $("#max_day_val").html(values[handle] + " d");
                    $("#max_day").val(values[handle]);
                }
            });
        }
        function setQualifiertView(qualifier, type , selected) {
            $(qualifier).html("");
            var qdropp = wfilters[type].qualifier;

            var select = $("<select name='qualifier' class='form-control qualifier'></select>");
            var selected_qualifier = "gteq";
            for (var key in qdropp) {
                if(key==selected_qualifier){
                    se= {selcted :'selcted'};
                }
                $('<option/>', {
                    'value': key,
                    'text': qdropp[key],
                }).appendTo(select);
            }
            select.val(selected);
            $(qualifier).append(select);
        }

        function setAmountView(rangervalue, type,selected_amount) {
            $(rangervalue).html("");
            $(".range_valueA").css("display", "none");
            $(".range_valueB").css("display", "none");

            if (rangervalue.noUiSlider) {
                rangervalue.noUiSlider.destroy();
            }

            if (wfilters[type].amount.rang) {
                var vrange = wfilters[type].amount.rang;
                $(".range_valueA").css("display", "block");
                if (vrange.range2) {
                    $(".range_valueB").css("display", "block");
                }


                noUiSlider.create(rangervalue, vrange.obj);

                rangervalue.noUiSlider.on('update', function (values, handle) {
                    if (handle == 0) {
                        $("#min_amount_val").html(values[handle] + " " + vrange.unit);
                        $("#min_amount").val(values[handle]);
                    } else {
                        $("#max_amount_val").html(values[handle] + " " + vrange.unit);
                        $("#max_amount").val(values[handle]);
                    }
                });
            } else if (wfilters[type].amount.dropdown) {
                var vopt = wfilters[type].amount.dropdown;

                var select = $("<select name='amount' class='form-control amount'></select>");
                for (var key in vopt) {
                    $('<option/>', {
                        'value': key,
                        'text': vopt[key]
                    }).appendTo(select);
                }
                select.val(selected_amount);
                $(rangervalue).append(select);
            }

        }
        
        var selected_qualifier = @if(isset($adsetting)) "{{$adsetting->qualifier}}"; @else "";  @endif
        var selected_ammount = @if(isset($adsetting)) "{{$adsetting->amount}}"; @else "";  @endif
        $('.filter_type').click(function () {
            $('#filter_container').css("display", "block");
            var type = $(this).attr("data-type");
            $("#weather_type").val(type);
            setAmountView(ammount_range_id, type,selected_ammount);
            setQualifiertView(qualifier_id, type,selected_qualifier);
        });
        @if($adsetting)
            var type = "{{$adsetting->weather_type}}";
            $('#filter_container').css("display", "block");
            $("#weather_type").val(type);
            
            @if($adsetting && $adsetting->min_amount )
            if (wfilters[type].amount.rang) {
                wfilters[type].amount.rang.obj.start[0]={{$adsetting->min_amount}};
                wfilters[type].amount.rang.obj.start[1]={{$adsetting->max_amount}};
            }
            @endif
            
            @if($adsetting && $adsetting->min_day )
                wday_range.start[0]={{$adsetting->min_day}};
            @endif
            setAmountView(ammount_range_id, type,selected_ammount);
            setQualifiertView(qualifier_id, type,selected_qualifier);
                
        @endif

        setDayRangeView(day_range_id)
    });

</script>

@endpush

@endsection