@extends('layouts.user-backend')

@section('title','Campaigns Adsets')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Campaigns Ads Settings</h5>
        <div class="heading-elements">
            {!! Form::select('campaign_id',$campaigns, null, ['class' => 'filter','id'=>'filter_campaign_id','style'=>'width:200px']) !!}
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">

                    <div class="table-responsive">
                        <table class="table  datatable-responsive-row-control datatable">

                            <thead>
                                <tr>
                                    <th data-priority="1">
                            <div class="btn-group">
                                <button type="button" class="btn bg-slate btn-icon btn-xs ">
                                    <input type="checkbox" class="styled select_all_adset_ids" style="margin: 0">
				</button>
                                <button type="button" class="btn bg-slate btn-xs modal_adsets_open" data-id="0" reload="0">Setting</button>
                                
                            </div>
                            </th>
                            <th >Ad Name</th>
                            <th >Location - Zip</th>
                            <th >Location Lat-Long</th>
                           <th data-priority="2" >Setting</th>
                            </tr>
                            </thead>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





@push('js')
<script>
    var url = "{{url('fb-adsets')}}";
    $("#filter_campaign_id").select2();
    
    $('.select_all_adset_ids').change(function (e) {
        var $this = $(this),
        child = $('.adset_ids');

         if ($this.is(':checked')) {
             child.prop('checked', true);
         } else {
             child.prop('checked', false);
         }
         e.preventDefault();
    });

    
    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [0, "desc"],
        columns: [
            {
                "data": null,
                "name": 'id',
                 "searchable": false,
                "orderable": false,
                "render": function (o) {
                    return '<input type="checkbox" name="adset_id[]" class="styled adset_ids"  value="'+o.id+'">';
                }
            },
            { "data": "name","name":"name"},
            { "data": "location_postalcode","name":"location_postalcode"},
            {
                "data": null,
                "name": 'location_longitude',
                "render": function (o) {
                    var location = "";
                    if(o.location_latitude && o.location_longitude){
                         var lat = parseFloat(o.location_latitude);
                         var lon = parseFloat(o.location_longitude);
                         location = lat.toFixed(2)+","+lon.toFixed(2);
                    }
                    return location;
                }
            },
            { 
                "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    var drop ="<a href='javascript:void(0);' data-id='"+o.id+"' reload='0' class='modal_adsets_open' ><i class='icon-cloud'></i> </a>";

                   
                    return drop;
                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('fb-adsets/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                 d.campaign_id = $('#filter_campaign_id').val();
                 $('.select_all_adset_ids').prop('checked', false);
            }
        }
    });
    
    $('.filter').change(function() {
       datatable.fnDraw();
    });
  
   
</script>
@endpush

@include('user-backend.compaigns.model_adset_option')

@endsection