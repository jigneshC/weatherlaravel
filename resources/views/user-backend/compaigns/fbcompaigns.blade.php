@extends('layouts.user-backend')

@section('title','Campaigns')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Campaigns</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">
                   {{-- <div class="panel-heading">
                        <h6 class="panel-title">Campaigns</h6>
                        <div class="heading-elements">
                            <span class="label bg-success heading-text">28 active</span>
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#"><i class="icon-sync"></i> Update data</a></li>
                                        <li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
                                        <li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-menu"></i></a>
                    </div>--}}

                    <div class="table-responsive">

                    </div>

                    <div class="table-responsive">
                        <table class="table text-nowrap">

                            <thead>
                                <tr>
                                    <th>Campaign</th>
                                    <th class="col-md-3">Check For Update</th>
                                    <th class="col-md-3">Add Lat/Long</th>
                                    <th class="col-md-3">Ads</th>
                                    <th class="col-md-3">Status</th>
                                    <th class="text-center" style="width: 20px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($campaigns as $campaign)
                                <tr class="active border-double">
                                    <td >{{$campaign->name}}</td>
                                    <td colspan="4" class="text-right"><span class="badge bg-blue"> @if($campaign->ads->count()>4 ) 5 @else {{$campaign->ads->count()}}  @endif / {{$campaign->ads->count()}} </span></td>
                                    <td class="text-right">
                                        
                                        <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><i class="icon-menu7"></i></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="{{url('fb-compaigns')}}/{{$campaign->campaign_id}}"><i class="icon-file-stats"></i> View All Ads</a></li>
                                                        <li><a href="{{url('fb-adsets')}}?campaign_id={{$campaign->campaign_id}}"><i class="icon-gear"></i> Settings</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        
                                    </td>
                                </tr>

                                @foreach($campaign->ads  as $k => $ad)
                                @if($k<5)
                                    <tr>
                                        <td>
                                            <div class="media-left">
                                                <div class=""><a href="#" class="text-default text-semibold"># {{$campaign->id}}</a></div>

                                            </div>
                                        </td>
                                        <td>
                                            <div  data-id="{{$ad->id}}" reload="1" class="media-body modal_adsets_open">
                                                <div class="media-body">
                                                    <a href="{{url('fb-ad-settings')}}/{{$ad->ad_id}}" class="display-inline-block text-default text-semibold letter-icon-title">
                                                        <i class="icon-gear"></i> @if($ad->adsettings) {{$ad->adsettings->weather_type}}  @endif
                                                    </a>
                                                    <div class="text-muted text-size-small">
                                                        @if($ad->next_check_time) 
                                                         <span class="status-mark border-blue position-left"></span>
                                                              {{ \Carbon\Carbon::parse($ad->next_check_time)->format("jS F Y h:i A")}}
                                                         @endif
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-info text-size-small"> 
                                                <a href="{{url('fb-adsets')}}?ad_id={{$ad->ad_id}}"><i class="icon-gear"></i>
                                                    @if($ad->location_latitude) {{ round($ad->location_latitude,3)}} - {{ round($ad->location_longitude,3) }} @else N/A @endif
                                                </a>
                                            </div>
                                        </td>
                                        <td><h6 class="text-semibold">{{$ad->name}}</h6></td>
                                        <td><span id="status_lable_{{$ad->id}}" class="label  @if($ad->status=='ACTIVE') bg-blue @else bg-danger @endif">{{$ad->status}}</span></td>
                                        <td class="text-center">
                                            <div class="checkbox checkbox-switchery switchery-sm">
                                                <label>
                                                    <input type="checkbox" class="switchery change_ad_status" data-id="{{$ad->id}}" @if($ad->status=='ACTIVE') data-status="ACTIVE" checked="checked" @else data-status="PAUSED" @endif>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                                @endforeach
                               
                            </tbody>
                        </table>
                        
                        {!! $campaigns->appends($_GET)->links() !!}
                    </div>
                </div>
                <!-- /marketing campaigns -->






            </div>


        </div>



    </div>
</div>


@endsection


@push('js')
<script>
    
    $('.change_ad_status').change(function (e) {
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        
         if ($(this).is(':checked') && status=="PAUSED") {
            $(this).attr("data-status","ACTIVE");
            changeAdstatus(id,"ACTIVE",$(this));
         } else if(status=="ACTIVE"){
            $(this).attr("data-status","PAUSED");
            changeAdstatus(id,"PAUSED",$(this));
         }
       
         
    });
    
    function changeAdstatus(id,status,self){
         var url = "{{url('fb-ad')}}/"+id;   
         $.ajax({
            type: "PUT",
            url: url,
            data:{'status':status},
            headers: {
                 "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (result) {
                $("#status_lable_"+id).removeClass("bg-blue");
                $("#status_lable_"+id).removeClass("bg-danger");
                 if(status=="ACTIVE"){
                    $("#status_lable_"+id).addClass("bg-blue");
                    $("#status_lable_"+id).html(status);
                }else{
                    $("#status_lable_"+id).addClass("bg-danger");
                    $("#status_lable_"+id).html(status);
                }
                toastr.success('Action Success!', result.message);
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro);
            }
        }); 
    }
</script>

@endpush


