@extends('layouts.user-backend')

@section('title','Campaign Detail')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Campaign Detail / {{$campaign->name}}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">
                   

                    <div class="table-responsive">

                    </div>

                    <div class="table-responsive">
                        <table class="table text-nowrap">

                            <thead>
                                <tr>
                                    <th># Add Id</th>
                                    <th class="col-md-3">Adset</th>
                                    <th class="col-md-3">Ads</th>
                                    <th class="col-md-3">Status</th>
                                    <th class="text-center" style="width: 20px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($ads as $k => $ad)
                                
                                    <tr>
                                        <td>
                                            <div class="media-left">
                                                <div class=""><a href="#" class="text-default text-semibold"># {{$ad->adset_id}}</a></div>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">{{$ad->adsets->name}}</a>
                                                <div class="text-muted text-size-small"><span class="status-mark @if($ad->adsets->status=='ACTIVE') border-blue @else border-danger @endif  position-left"></span> {{$ad->adsets->status}}</div>
                                            </div>
                                        </td>
                                        <td><h6 class="text-semibold">{{$ad->name}}</h6></td>
                                        <td><span class="label @if($ad->status=='ACTIVE') bg-blue @else bg-danger @endif">{{$ad->status}}</span></td>
                                        <td class="text-center">
                                            
                                        </td>
                                    </tr>
                               
                                @endforeach
                                
                               
                            </tbody>
                        </table>
                        
                        {!! $ads->appends($_GET)->links() !!}
                    </div>
                </div>
                <!-- /marketing campaigns -->






            </div>

           
        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


