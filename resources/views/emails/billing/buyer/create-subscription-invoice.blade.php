<!DOCTYPE html>
<html>
	@include ('emails.include.header', ['lang' => $lang])
	<body style="margin:30px auto; ">
		<div style="background:#000; padding:20px; text-align:center;">
			<img class="brand_icon" src="{{ asset('/frontend/images/logo.png') }}" alt="logo"/>
		</div>
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
                        <h2>@lang('billing.mail_header_lines.user_invoice_subscription1',['item_name'=>$package->name,'from'=>\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$transaction->period_start_date)->format("jS M, Y"),'to'=>\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$transaction->period_end_date)->format("jS M, Y")],$lang)</h2>
                        @include ('emails.include.order-table', ['lang' => 'en'])
			<h5 style="text-align: center;margin-top: 55px;font-size: 15px;color: #555;">
			</h5>
		</div>
		@include ('emails.include.footer',['lang' => $lang])
	</body>
</html>
