<table>
    <tr>
        <th>Billing Id</th>
        <th>Package Name</th>
        <th>Package Detail</th>
        <th>Package Class</th>
        <th>Compaigns</th>
        <th>Price / Duration</th>



    </tr>
    <tr>
        <td>{{ $billing->subscription_id}}</td>
        <td>{{  $package->name}}</td>
        <td>{{  $package->desc}}</td>
        <td>{{  $package->package_class}}</td>
        <td>{{  $package->unit}}</td>
        <td>${{  $package->price }} / {{  $package->interval}}</td>
    </tr>
</table>


<h4 style="margin-top: 35px;">Buyer Detail</h4>
<table class="buyer_detail">
    <tr style="background-color:#f2f2f2">
        <th>Buyer Name</th>
        <td>{{$billing->buyer_name}}</th>
    </tr>
    <tr>
        <th>Buyer Email</th>
        <td>{{$billing->buyer_email}}</td>
    </tr>
    <tr>
        <th>Buyer Contact</th>
        <td>{{$billing->buyer_contact}}</td>
    </tr>

</table>

<h4 style="margin-top: 35px;">Payment Detail</h4>
<table class="buyer_detail">
    <tr>
        <th>Price / Duration</th>
        <td>${{  $package->price }} / {{  $package->interval}}</td>
    </tr>
    @if($transaction)
    <tr>
        <th>Invoice Id</th>
        <td>{{  $transaction->invoice_id }}</td>
    </tr>
    <tr>
        <th>Transaction Id</th>
        <td>{{  $transaction->charge_id }}</td>
    </tr>
    <tr>
        <th>Billing Cycle</th>
        <td>{{  $transaction->period_start_date }} - {{  $transaction->period_end_date}}</td>
    </tr>
    @endif
</table>

