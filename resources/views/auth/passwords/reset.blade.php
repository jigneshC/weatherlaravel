@extends('layouts.themeb_auth')
@section('title','Reset Password')
@section('body_class','login-bg')

@push('css')
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
@endpush

@section('content')


<section class="header-section other-header-section">
    <div class="login-container">
        <div class="login-top-container">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-lg-7 col-md-9 col-sm-12">

                        <div class="content_div">
                            <div class="logo-div fadeInLeft animated"><a href="{{url('')}}"><img src="{!! asset('themeb/images/logo.png') !!}" alt=""></a></div> 
                        </div>	

                        <div class="card_div">
                            <div class="row justify-content-md-center">
                                <div class="col-lg-7 col-md-7 col-sm-10">
                                    <div class="login-div-content">
                                        <h2>Reset Password </h2>
                                    </div>
                                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                                    {{ csrf_field() }}
                                         <input type="hidden" name="token" value="{{ $token }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email...">
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="******">
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPasswordCon">Confirm Password</label>
                                            <input type="password" name="password_confirmation" class="form-control" id="exampleInputPasswordCon" placeholder="Confirm Password">
                                            @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="btn-div">
                                            <button class="btn btn-startfree" type="submit">Reset Password</button>
                                        </div>
                                    </form>
                                </div>	
                            </div>
                        </div>

                    </div><!-- end of col -->
                </div>
            </div>
        </div>
    </div><!-- end of contact-top-container -->

</section>


@endsection





                       

                       

                   
