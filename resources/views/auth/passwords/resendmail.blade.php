
@extends('layouts.themeb_auth')
@section('title','Resend Activation Mail')
@section('body_class','login-bg')

@push('css')
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
@endpush

@section('content')


<section class="header-section other-header-section">
    <div class="login-container">
        <div class="login-top-container">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-lg-7 col-md-9 col-sm-12">

                        <div class="content_div">
                            <div class="logo-div fadeInLeft animated"><a href="{{url('')}}"><img src="{!! asset('themeb/images/logo.png') !!}" alt=""></a></div> 
                        </div>	

                        <div class="card_div">
                            <div class="row justify-content-md-center">
                                <div class="col-lg-7 col-md-7 col-sm-10">
                                    <div class="login-div-content">
                                        <h2>Resend Activation Mail </h2>
                                    </div>
                                    @if (session('flash_success'))
                                    <div class="alert alert-success">
                                        {{ session('flash_success') }}
                                    </div>
                                    @endif
                                    @if (session('flash_error'))
                                    <div class="alert alert-success">
                                        {{ session('flash_error') }}
                                    </div>
                                    @endif
                                     <form class="form-horizontal" method="POST" action="{!! url('/resend-activation') !!}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email...">
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>


                                        <div class="btn-div">
                                            <button class="btn btn-startfree" type="submit">SEND</button>
                                        </div>


                                    </form>

                                </div>	
                            </div>
                        </div>

                    </div><!-- end of col -->
                </div>
            </div>
        </div>
    </div><!-- end of contact-top-container -->

</section>


@endsection

