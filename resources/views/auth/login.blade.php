@extends('layouts.themeb_auth')
@section('title','Login')
@section('body_class','login-bg')

@push('css')
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
@endpush


@section('content')

<section class="header-section other-header-section">
		<div class="login-container">
			<div class="login-top-container">
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-lg-7 col-md-9 col-sm-12">
							
							<div class="content_div">
								<div class="logo-div fadeInLeft animated"><a href="{{url('')}}"><img src="{!! asset('themeb/images/logo.png') !!}" alt=""></a></div> 
							</div>	
							
							<div class="card_div">
								<div class="row justify-content-md-center">
									<div class="col-lg-7 col-md-7 col-sm-10">
										<div class="login-div-content">
											<h2>Sign In </h2>
                                                                                        @if(Session::has('is_active_error'))
                                                                                        <div class="alert alert-warning">Your Account Has Not Activated Yet. </div>
                                                                                        <strong>If you have not received activation email <a href="{!! url('/resend-activation') !!}">Click here to resend Activation Email.</a></strong>
                                                                                        @else
											<p>Don't have an account? <a href="{{ route('register') }}"> Sign up </a> </p>
                                                                                        @endif
										</div>
										<form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                                                                    {{ csrf_field() }}
											<div class="form-group">
												<label for="exampleInputEmail1">Email</label>
												<input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email...">
                                                                                                 @if ($errors->has('email'))
                                                                                                    <span class="help-block">
                                                                                                        <strong>{{ $errors->first('email') }}</strong>
                                                                                                    </span>
                                                                                                @endif
											</div>
											<div class="form-group">
												<label for="exampleInputPassword1">Password</label>
												<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="******">
                                                                                                 @if ($errors->has('password'))
                                                                                                    <span class="help-block">
                                                                                                        <strong>{{ $errors->first('password') }}</strong>
                                                                                                    </span>
                                                                                                 @endif
											</div>
											<div class="forgot-password">
												<a href="{{ route('password.request') }}">Forgot Password?</a>
											</div>
											
											<div class="btn-div">
												<button class="btn btn-startfree" type="submit">Login to your account</button>
												{{-- <p>or</p>
												<button class="btn btn-facebook" type="submit"><span><i class="fa fa-facebook-square"></i>Login with facebook</span></button> --}}
											</div>
										</form>
									</div>	
								</div>
							</div>
							
						</div><!-- end of col -->
					</div>
				</div>
			</div>
		</div><!-- end of contact-top-container -->
	
	</section>


     
@endsection

