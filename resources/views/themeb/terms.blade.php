@extends('layouts.themeb')

@section('title','Terms & Privacy - Trigger Ads')

@section('page_id','term')
@section('content')

<section class="terms-section">
		<div class="terms-div clearfix">
                    <h2 class="blk-title ">Terms & Privacy</h2>
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 slideInLeft animated">
						<p>Trigger Ads</p>
						<p>Version :[March] 2016</p>
						<p>TERMS AND CONDITIONS OF SALE</p>
						<p>This page (together with our Privacy Policy, Terms of Website Use and Website Acceptable Use Policy tells you information about us and the legal terms and conditions ("Terms")
							on which we sell any of the products ("Products") listed on our website ("our site") to you whether through the website or otherwise.</p>
						<p>These Terms will apply to any contract between Holly & Beau Limited("Us") for the sale of Products to you ("Contract"). Please read these Terms carefully and make sure that
							you understand them, before ordering any Products from our site. Please note that by ordering any of our Products, you agree to be bound by these Terms and the other
							documents expressly referred to in it</p>
						<p>Please click on the button marked "I Accept" at the end of these Terms if you accept them. If you refuse to accept these Terms, you will not be able to order any Products from our
							site.</p>	
						<p>You should print a copy of these Terms for future reference.</p>	
						<p>We amend these Terms from time to time as set out in clause 8. Every time you wish to order Products, please check these Terms to ensure you understand the terms which will
							apply at that time.</p>
						<p>These Terms, and any Contract between us, are only in the English language.</p>	
						<p>Information about us</p>
						<p><span>1.1</span> We operate the website www.hollyandbeau.com. We are Holly & Beau Limited, a company registered in England and Wales under company number 08143964 and with
							our registered office at Rouen House, Rouen Road, Norwich NR1 1RB. Our main trading address is Rouen House, Rouen Road, Norwich, NR1 1RB Our VAT number is 140 0202 91.</p>
						
						<p><span>1.2</span> To contact Us, please see our Contact Us page</p>
						<p>Our Products</p>
						<p><span>2.1</span> The images of the Products on our site are for illustrative purposes only. Although we have made every effort to display the colours accurately, we cannot guarantee
							that your computer's display of the colours accurately reflect the colour of the Products. Your Products may vary slightly from those images.</p>
						<p><span>2.2</span> Although we have made every effort to be as accurate as possible, all sizes, weights, capacities, dimensions and measurements indicated on our site have a 2%
							tolerance.</p>
						<p><span>2.3</span> The packaging of the Products may vary from that shown on images on our site.</p>
						<p><span>2.4</span> All Products shown on our site are subject to availability. We will inform you by e-mail as soon as possible if the Product you have ordered is not available and we will
							not process your order if made.administrator is given or if an administrator is appointed over you;</p>
						<p><span>2.5</span> (being a company) the holder of a qualifying floating charge over your assets has become entitled to appoint or has appointed an administrative receiver;</p>	
					</div>
				</div>
			</div>
		</div>
	</section>



@endsection


@push('js')
<script>
    </script>
@endpush


