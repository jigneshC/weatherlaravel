@extends('layouts.themeb')

@section('title','Pricing')

@section('page_id','pricing')
@section('body_class','')
@section('content')

<section class="faq-section">
	<div class="faq-div clearfix">
		<h2 class="blk-title ">FAQ</h2>
	
		<div class="container">
		<div class="row">
		
			
			
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
		
		
			
		</div>
		</div>
		</div><!-- end of faq-div -->
	</section>



@endsection


@push('js')
<script>
    
    // With JQuery
$("#price").slider({
    ticks: [500, 1000, 3000, 5000, 7500, 10000, 11000],
    ticks_labels: ['&pound;500', '&pound;1k', '&pound;3k', '&pound;5k', '&pound;7.5k', '&pound;10k', '&pound;10k+'],
    ticks_snap_bounds: 30
});
$("#price-year").slider({	
    ticks: [500, 1000, 3000, 5000, 7500, 10000, 11000],
    ticks_labels: ['&pound;500', '&pound;1k', '&pound;3k', '&pound;5k', '&pound;7.5k', '&pound;10k', '&pound;10k+'],
    ticks_snap_bounds: 30,
	formatter: function(val) {
					this.tickLabelContainer.appendChild(label)
				}
	
});


    </script>
@endpush


