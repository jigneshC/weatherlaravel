<div class="banner-container clearfix">
	
		<div class="banner-div clearfix fadeInLeft animated">
		
			<h3>Weather triggers for Facebook Ads.</h3>
			<h4>Optimise your ad display and boost your ROAS with our weather trigger tool.</h4>
			
                        
			<div class="top-email-box clearfix">
				<form class="form-inline" action="register">
						  <input class="form-control" name="email" type="text" placeholder="Enter your Email..." aria-label="Enter your Email...">
						  <button class="btn btn-startfree " type="submit">Start Free</button>
						</form>
				
			</div>
                        
			
		
		</div><!-- end of banner-div -->
	
	</div><!-- end of banner-container -->
	<div class="dashboard-mb-img fadeInRight animated"><img src="{!! asset('themeb/images/Dashboard.png') !!}" alt=""></div>
	
	<div class="three-action-box slideInLeft animated clearfix">
		<div class="container">
		<div class="row">
		
			<div class="col-md-4 col-sm-4">
				<div class="three-action-div clearfix">
					<h3>Facebook Ads Optimistion</h3>
					<p>Nulla posuere neque sed ex pellentesque ultrices. Donec iaculis felis tempor, auctor ipsum et, egestas velit. Morbi laoreet orci at blandit iaculis.</p>
				</div>
			</div><!-- end of col -->
			<div class="col-md-4 col-sm-4">
				<div class="three-action-div clearfix">
					<h3>Multiple weather triggers</h3>
					<p>Nulla posuere neque sed ex pellentesque ultrices. Donec iaculis felis tempor, auctor ipsum et, egestas velit. Morbi laoreet orci at blandit iaculis.</p>
				</div>
			</div><!-- end of col -->
			<div class="col-md-4 col-sm-4">
				<div class="three-action-div clearfix">
					<h3>Increased ROAS</h3>
					<p>Nulla posuere neque sed ex pellentesque ultrices. Donec iaculis felis tempor, auctor ipsum et, egestas velit. Morbi laoreet orci at blandit iaculis.</p>
				</div>
			</div><!-- end of col -->
		
		</div>
		</div>
	
	</div><!-- end of three-action-box -->
	
	<div class="short-plan-div slideInRight animated clearfix">
		<div class="short-plan-box clearfix">
			<div class="txt-box">
				<h3>Try out our 100% Free package</h3>
				<p>We increase our clients ROADs by 26%</p>
			</div>
			<div class="btn-link-div">
				<a href="#" class="link"></a>
			</div>
		</div><!-- end of short-plan-box -->
	</div><!-- end of short-plan-div -->