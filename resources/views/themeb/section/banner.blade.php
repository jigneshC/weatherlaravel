<div class="banner-container">
    
    <div class="banner-vidoe-img-div">
        
            <div class="video-background">
                    <div class="video-foreground">
                            <video  id="bgvid" playsinline autoplay muted loop>
                                <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
                              <source src="{!! asset('/uploads/adobestock_118514568.mov') !!}" type="video/webm">
                              <source src="{!! asset('/uploads/adobestock_118514568.mov') !!}" type="video/mp4">
                               <!--source src="http://thenewcode.com/assets/videos/polina.webm" type="video/webm">
                               <source src="http://thenewcode.com/assets/videos/polina.mp4" type="video/mp4"-->
                              </video>
                        <!--iframe width="100%" height="100%" src="https://www.youtube.com/embed/MeTQplAmuTU?controls=0&showinfo=0&autoplay=1&autohide=0&iv_load_policy=3&mute=1&modestbranding=1&yt:strech=16:9&loop=1&vq=hd1080&hd=1&playlist=MeTQplAmuTU&disablekb=1&cc_load_policy=1&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe-->
                        
                    </div>
                    </div> 
        
        <img src="{!! asset('/frontend/images/hero-img.jpg') !!}" alt="" class="img-responsive">
    
    </div>
    <div class="banner-btm-img"></div>

    <div class="banner-caption-div clearfix">
            <h2><span>Weather</span> triggered advertising for <span>social media</span>.</h2>
            @if (!Auth::check())
            <div class="btn-link"><a href="{{url('register')}}">Sign up for a FREE trial</a></div>
            @endif
            
        </div>


</div>