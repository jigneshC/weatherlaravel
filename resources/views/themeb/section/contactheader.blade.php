<div class="contact-container">
			<div class="contact-top-container">
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-lg-7 col-md-9 col-sm-12">
							<div class="content_div">
								<h1>Talk to us.</h2>
								<h3>Please fill out the simple form below and we'll get back to you within 24 hours. </h3>
							</div>	
							<div class="card_div">
								<form>
									<div class="form-group">
										<label for="exampleInputEmail1">Email</label>
										<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email...">
									</div>
									<div class="form-group">
										<label for="exampleInputPassword1">Password</label>
										<input type="password" class="form-control" id="exampleInputPassword1" placeholder="******">
									</div>
									<div class="form-group">
										<label for="exampleFormControlTextarea1">Now let's hear the details</label>
										<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Let us know what you need"></textarea>
									</div>
									<div class="btn-div">
										<button class="btn btn-startfree" type="submit">Submit</button>
									</div>
								</form>
							</div>
						</div><!-- end of col -->
					</div>
				</div>
			</div>
		</div>