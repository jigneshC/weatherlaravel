 <section class="download-app-section">
		<div class="download-app-div clearfix">
		
			<h3>Download the app.</h3>
			<p>Download the app on the App Store or Google play store and start optimising your ads today.</p>
			
			<div class="app-btn slideInUp animated">
				<a href="#"><img src="{!! asset('themeb/images/app-btn.png') !!}" alt="" class="img-responsive"></a>
			</div>
			
		</div><!-- end of get-download-app-div -->
	
	</section><!-- end of get-download-app-section -->