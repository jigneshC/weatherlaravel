<section class="features-section feature-top-1 light-red-bg-1">
	
		<div class="container">
		<div class="row">
		
			<div class="features-div clearfix">
			
			<div class="col-md-6 col-sm-6 pull-left slideInLeft animated">
				<div class="features-img-div clearfix">
					<span class="img1"><img src="{!! asset('/themeb/images/facebook-permissions.png') !!}" alt=""></span>
					<span class="img2"><img src="{!! asset('/themeb/images/dashboard-full.png') !!}" alt=""></span>
				</div>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6  pull-right slideInRight animated">
				<div class="features-action-div clearfix">
					<h3>Easily optimise all of your social media ads using weather triggers.</h3>
					<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla posuere neque sed ex pellentesque ultrices.</p>
					<p>Fusce hendrerit sollicitudin odio, nec iaculis mauris ornare condimentum. Donec varius lobortis magna, id vestibulum dolor tristique in.</p>
				</div>
			</div><!-- end of col -->
			
		
		
			</div><!-- end of features-div -->
		</div>
		</div>
		
	</section><!-- end of features-section -->
        
        
        
        <section class="features-section feature-2">
	
		<div class="container">
		<div class="row">
		
			<div class="features-div clearfix">
			
			<div class="col-md-6 col-sm-6 pull-right slideInLeft animated">
				<div class="features-img-div right clearfix">
					<span class="img1"><img src="{!! asset('/themeb/images/dashboard-dropdown.png') !!}" alt=""></span>
					<span class="img2"><img src="{!! asset('/themeb/images/dashboard.png') !!}" alt=""></span>
				</div>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6  pull-left slideInRight animated">
				<div class="features-action-div clearfix">
					<h3>Easily optimise all of your social media ads using weather triggers.</h3>
					<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla posuere neque sed ex pellentesque ultrices.</p>
					<p>Fusce hendrerit sollicitudin odio, nec iaculis mauris ornare condimentum. Donec varius lobortis magna, id vestibulum dolor tristique in.</p>
				</div>
			</div><!-- end of col -->
			
		
		
			</div><!-- end of features-div -->
		</div>
		</div>
		
	</section><!-- end of features-section -->
        
        
        <section class="features-section light-red-bg-2 feature-top-2">
	
		<div class="container">
		<div class="row">
		
			<div class="features-div clearfix">
			
			<div class="col-md-6 col-sm-6 pull-left slideInLeft animated">
				<div class="features-img-div clearfix">
					<span class="img1"><img src="{!! asset('themeb/images/facebook-permissions.png') !!}" alt=""></span>
					<span class="img2"><img src="{!! asset('themeb/images/dashboard-full.png') !!}" alt=""></span>
				</div>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6  pull-right slideInRight animated">
				<div class="features-action-div clearfix">
					<h3>Easily optimise all of your social media ads using weather triggers.</h3>
					<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla posuere neque sed ex pellentesque ultrices.</p>
					<p>Fusce hendrerit sollicitudin odio, nec iaculis mauris ornare condimentum. Donec varius lobortis magna, id vestibulum dolor tristique in.</p>
				</div>
			</div><!-- end of col -->
			
		
		
			</div><!-- end of features-div -->
		</div>
		</div>
		
	</section><!-- end of features-section -->
        
        
        <section class="video-section">
		<div class="video-div">
			<h3>Optimised the way you adertise your products.</h3>
			<p>See how Trigger can improve your ROAS by only promoting your products when the weather is right to do so.</p>
			
			<div class="vidoe-icon slideInUp animated">
				<a href="#"><img src="{!! asset('themeb/images/video-icon.png') !!}" alt="" class="img-responsive"></a>
			</div>
			
		</div>
	
	</section><!-- end of video-section -->
        
        
        <section class="get-started-section ">
		<div class="get-started-div">
		
			<div class="get-started-img slideInDown animated">
				<img src="{!! asset('themeb/images/laptop-mobile.png') !!}" alt="" class="img-responsive">
			</div>
		
			<h3>Get Started today</h3>
			<p>If you want to start optimising your ad campaign based on weather then look no further then Trigger Ads.</p>
			
			<div class="email-box clearfix">
				<form class="form-inline" action="register">
						  <input class="form-control" type="text" name="email" placeholder="Enter Email..." aria-label="Enter Email...">
						  <button class="btn btn-startfree " type="submit">Start Free</button>
						</form>
                            {{-- <span class="btm-link">or <a href="#">sign up with facebook</a></span> --}}
				
			</div>
			
			
		</div>
	
	</section><!-- end of get-started-section -->
        
        
       