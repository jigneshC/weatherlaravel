@extends('layouts.themeb')

@section('title','Faq')

@section('page_id','faq')
@section('content')

<section class="terms-section">
		<div class="terms-div clearfix">
                    <h2 class="blk-title ">FAQ</h2>
			<div class="container">
		<div class="row">
		
			
			
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h3>
					<p>Sed ac lectus nisi. Maecenas ut aliquam nunc. Duis rhoncus ex eros, vel tempor orci porttitor eget. Sed interdum ex ac lacus aliquet pharetra. Ut vel accumsan urna. Integer ex nunc, porttitor non hendrerit nec, pellentesque a nisl. Nullam erat elit, auctor eu tortor quis, blandit aliquet sapien. Donec pellentesque congue felis in interdum. </p>
				
				</div>
			</div><!-- end of col -->
			
		
		
			
		</div>
		</div>
		</div>
	</section>



@endsection


@push('js')
<script>
    </script>
@endpush


