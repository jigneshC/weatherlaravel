<footer>
		<div class="footer-div">
		
		<div class="footer-col-div clearfix">
		
			<div class="container">
			<div class="row">
			
			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<img src="{!! asset('themeb/images/footer-logo.png') !!}" alt="" class="img-responsive footer-logo">
				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->
			
			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<h4>Learn more</h4>
					
					<ul>
						<li><a href="#">Download ios App</a></li>
						<li><a href="{{url('terms-and-privacy')}}">Terms & Privacy</a></li>
					</ul>
					
				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->
			
			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<h4>Support</h4>
					
					<ul>
						<li><a href="{{url('contact-us')}}">Contact Us</a></li>
						<li><a href="{{url('faq')}}">FAQ</a></li>
					</ul>
					
				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->
			
			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<h4>Address</h4>
					
					<p>East Technology Ltd.</p>
					<p>The Union Building 51-59<br>Rose Land NORWICH<br>
					Norfolk, NRI IBY
					</p>
					
				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->
			
			
			</div><!-- end of row -->
			</div><!-- end of container -->
		
		</div><!-- end of footer-col-div -->
		</div><!-- end of footer div -->
		
		<div class="copyright-div clearfix">
			<div class="container">
			<div class="row">
			
			
			<div class="col-md-6 col-sm-6 pull-left">
                             <?php $today = getdate(); ?>
				<p>Copyright &copy; {{$today['year']}} {{ config('app.name') }} </p>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6 pull-right">
				<ul class="social-footer">
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>					
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div><!-- end of col -->
			
			

		</div><!-- end of copyright-div -->
		
	</footer>
