<section class="header-section">
	<header>
		<div class="header-div clearfix">
			<div class="container-fluid">
			<div class="row">
			<div class="col-md-12 col-sm-12">
			
				<div class="logo-div fadeInLeft animated"><a href="{{url('')}}"><img src="{!! asset('themeb/images/logo.png') !!}" alt=""></a></div>
				
				<div class="nav-div fadeInDownBig animated">
					<nav class="navbar navbar-expand-lg">
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>

					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
						  <li class="nav-item {{ request()->is('features') ? 'active' : '' }}">
							<a class="nav-link " href="{{url('features')}}">Features</a>
						  </li>
						  <li class="nav-item {{ request()->is('pricing') ? 'active' : '' }}">
							<a class="nav-link " href="{{url('pricing')}}">Pricing</a>
						  </li>
						  <li class="nav-item {{ request()->is('contact-us') ? 'active' : '' }}">
							<a class="nav-link " href="{{url('contact-us')}}">Contact</a>
						  </li>
						  
                                                  @if (Auth::check())
                                                    <li class="nav-link {{ request()->is('social-sync') ? 'active' : '' }}"><a href="{{url('social-sync')}}">My Campaigns</a></li>

                                                    <li class="nav-link"> <a href="#" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();" > Logout   </a> 
                                                    </li>
                                                    @else
                                                       <li class="nav-item {{ request()->is('login') ? 'active' : '' }}">
                                                            <a class="nav-link " href="{{url('login')}}">Login</a>
                                                       </li>
                                                    @endif
						  
						</ul>
                                              @if (!Auth::check())
						<form class="form-inline top-startup"  action="register">
						  <input name="email" class="form-control" type="text" placeholder="Enter Email..." aria-label="Enter Email...">
						  <button class="btn btn-startfree " type="submit">Start Free</button>
						</form>
                                              @endif
                                              @if (Auth::check())
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST"style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                            @endif
					  </div>
					</nav>
				</div><!-- end of nav div -->
			
			</div>
			</div>
			</div>
		
		</div><!-- end of header-div -->
	</header>
	
        @if(View::hasSection('page_id') && app()->view->getSections()['page_id']=="home")
            @include('themeb.section.homeheader')
        @elseif(View::hasSection('page_id') && app()->view->getSections()['page_id']=="pricing")
            @include('themeb.section.pricingheader')
         @elseif(View::hasSection('page_id') && app()->view->getSections()['page_id']=="contact")
            @include('themeb.section.contactheader')    
        @else    
            @php( $page_id =  "")
        @endif
	
	</section><!-- end of header-section -->