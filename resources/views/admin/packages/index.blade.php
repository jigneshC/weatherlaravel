@extends('layouts.admin')
@section('title','Package')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                            <i class="icon-circle-blank"></i>
                            Packages
                    </div>
                </div>
                <div class="box-content ">
                    <a href="#" class="btn btn-success btn-sm unitpackage_form_open" data-id="0" title="Add New Packages" >
                        <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                    </a>
                </div>
                <div class="box-content ">
                    <div class="row">
                        
                        <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" width="100%">
                                <thead>
                                <tr>
                                    <th data-priority="7">#Stripe Id</th>
                                    <th data-priority="1">Name</th>
                                    <th data-priority="2">Price</th>
                                    <th data-priority="3">Billing Cycle</th>
                                    <th data-priority="4">Package Class</th>
                                    <th data-priority="6">Description</th>
                                    <th data-priority="5">Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        </div>
                       
                    </div>
                    </div>
                </div>
        </div>
    </div>
    @include("admin.packages.packageForm")
@endsection
@push('js')
<script>
    var url = "{{url('admin/packages')}}";
    $("#action").select2();
    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [0, "desc"],
        columns: [
            {
                "data": null,
                "name": 'stripe_product_id',
                "searchable": false,
                "render": function (o) {
                    if(o.stripe_product_id && o.stripe_product_id!=""){
                        return o.stripe_product_id;
                    }else{
                        return '<a href="'+url+'/generate-stripe-plan/'+o.id+'" title="Generate Stipe Plan"><span class="text-red status-label bg-green">Generate Stripe Plan</span></a>';
                    }
                }
            },
            { "data": "name","name":"name"},
            {
                "data": null,
                "name": 'price',
                "searchable": false,
                "render": function (o) {
                    return o.price+" "+o.currency;
                }
            },
            {
                "data": null,
                "name": 'interval_count',
                "searchable": false,
                "render": function (o) {
                    return o.interval_count+" "+o.interval;
                }
            },
            { "data": "package_class","name":"package_class"},
            { "data": "desc","name":"desc"},
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    var e=""; var d=""; var v="";

                   
                        e= "<a href='javascript:void(0);' data-id='"+o.id+"' class='unitpackage_form_open' title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                   
                   
                        d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')' ><i class='fa fa-trash action_icon '></i></a>";
                   
                    var v =  "";
                    return v+d+e;
                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/packages/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {

            }
        }
    });
  
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Package'])");
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush

