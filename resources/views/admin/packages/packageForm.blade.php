<div class="modal modal-static fade" id="unitpackage_modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title_form_model"></h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="row rental_option_form_row">
                        {!! Form::open(['url' => '','id'=>'unitpackage_form','class' => 'form-horizontal unitpackage_form', 'files' => true]) !!}

                        {!! Form::hidden('package_id',0, ['class' => 'form-control','id'=>'package_id']) !!}
                        
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', 'Name *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                    <input type="text" name="name" class="filter form-control"  id="name" style="">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
                            {!! Form::label('desc', 'Description *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                    <input type="text" name="desc" class="filter form-control"  id="desc" style="">
                                {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                            {!! Form::label('price', 'Price *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                    <input type="number" name="price" class="filter form-control" min="0" id="price" style="">
                                {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('currency') ? 'has-error' : ''}}">
                            {!! Form::label('currency', 'Currency *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                    {!! Form::select('currency',['USD'=>'USD'],null, ['class' => 'full-width','id'=>'currency']) !!}
                                {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('package_class') ? 'has-error' : ''}}">
                                {!! Form::label('package_class', 'Package Class *',['class' => 'col-md-4 control-label']) !!}
                                <div class="col-md-6">
                                        <select class="full-width filter" id="package_class" name="package_class">
                                                @foreach(\config('settings.package_class') as $k => $val)
                                                    <option value="{{$k}}">{{$k}}</option>
                                                @endforeach
                                        </select>
                                    {!! $errors->first('package_class', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                     {{--   <div class="form-group {{ $errors->has('interval_count') ? 'has-error' : ''}}">
                            {!! Form::label('interval_count', 'Payment Cycle Count *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                <input type="number" name="interval_count" class="filter form-control" min="0" id="interval_count" style="">
                                {!! $errors->first('interval_count', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>--}}

                        <div class="form-group {{ $errors->has('interval') ? 'has-error' : ''}}">
                            {!! Form::label('interval', 'Billing Cycle *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::select('interval',['week'=>"Week" ,'month'=>'Month','year'=>'Year'] ,null, ['class' => 'form-control']) !!}                     
                                {!! $errors->first('interval', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        
                        <div class="form-group newImageArea {{ $errors->has('image') ? 'has-error' : ''}}">
                                {!! Form::label('image', 'Image *', ['class' => 'col-md-4 control-label']) !!}
                                <div class="col-md-6" >
                                    <input type="file" name="image" id="image" >
                                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                                </div>
                        </div>
                        
                        <div id="imageArea">
                            
                        </div>
                        
                        <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
                            {!! Form::label('details', 'Details *', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::textarea('details[0]', null, ['class' => 'form-control textarea textarea_0']) !!}
                                <input type="hidden" id="detailBoxCount" value="0">
                                {!! $errors->first('details','<p class="help-block with-errors">:message</p>') !!}
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-success btn-sm pull-left" id="addDeatils" title="Add Details">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>

                        <div class="allDetailsDiv">
                            
                        </div>

                        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                            {!! Form::label('status', 'Status *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::select('status',['' =>'--select--', 'active'=>'Active','inactive'=>'Inactive'] ,null, ['class' => 'form-control']) !!}
                                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <p class="form_submit_error text-error"></p>

                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-primary btn_form_model']) !!}

                        <button type="button" class="btn btn-warning" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('js')


<script>
$(document).ready(function(){ 
    $("#addDeatils").click(function(){
        var detailBoxCount = $("#detailBoxCount").val();  
        var newCount = $.trim((parseInt(detailBoxCount) + 1)); 
        var id = 'textarea_'+(parseInt(detailBoxCount) + 1);  
        var addString = '';
        var addString = "<div class='form-group "+id+"'>\
                <div class='col-md-4'> <lable class='control-label'></lable> </div>\
                <div class='col-md-6'>\
                        <textarea class='form-control textarea "+id+"' name='details["+newCount+"]'></textarea>\
                </div>\
            <div class='col-md-2'>\
                <button type='button' class='btn btn-success btn-sm pull-left' id='"+id+"' onclick=\'RemoveBox(this.id)\' title='Remove Details'>\
                    <i class='fa fa-minus' aria-hidden='true'></i>\
                </button>\
            </div>\
        </div>";
        $(".allDetailsDiv").append(addString);    
        $("#detailBoxCount").val(newCount);                 
    });
});
function RemoveBox(id){
    var idName = '.'+id;
    $(idName).remove();   
    var detailBoxCount = $("#detailBoxCount").val();  
    var newCount = $.trim((parseInt(detailBoxCount) - 1)); 
    $("#detailBoxCount").val(newCount);       
};
/***************action model******************/
    var _model = "#unitpackage_modal";
        $(document).on('click', '.unitpackage_form_open', function (e) {
            $(".unitpackage_form")[0].reset();
            $("#package_class")[0].selectedIndex = 0;
            $("#currency")[0].selectedIndex = 0;
            $("#interval")[0].selectedIndex = 0;
            $("#status")[0].selectedIndex = 0;
            $(".allDetailsDiv").html(''); 
            $("#imageArea").html(''); 
            $('.newImageArea').show();
            $("#detailBoxCount").val(0);  
            var id=$(this).attr('data-id');            
            $("#package_id").val(id);
            if(id!=0){
                $(".title_form_model").text("Update Package");
                var url = "{{url('admin/packages')}}/"+id+"/edit";   
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (result) {
                        var data=result.data;
                        
                        var detailsArray = jQuery.parseJSON( data.details );
                        $(".unitpackage_form #name").val(data.name);
                        $(".unitpackage_form #desc").val(data.desc);
                        $(".unitpackage_form #price").val(data.price);
                       // $(".unitpackage_form #interval_count").val(data.interval_count);
                        $('#currency [value="'+data.currency+'"]').attr('selected', 'true');
                        $('#package_class [value="'+data.package_class+'"]').attr('selected', 'true');
                        $('#interval [value="'+data.interval+'"]').attr('selected', 'true');
                        $('#status [value="'+data.status+'"]').attr('selected', 'true');
                        $(".unitpackage_form .textarea_0").val(detailsArray[0]);
                        $(".allDetailsDiv").html('');   
                        for (var index = 1; index < detailsArray.length; index++) {

                            var detailBoxCount = $("#detailBoxCount").val();  
                            var newCount = $.trim((parseInt(detailBoxCount) + 1)); 
                            var id = 'textarea_'+(parseInt(detailBoxCount) + 1);  
                            var addString = '';
                            var addString = "<div class='form-group "+id+"'>\
                                    <div class='col-md-4'> <lable class='control-label'></lable> </div>\
                                    <div class='col-md-6'>\
                                            <textarea class='form-control textarea "+id+"' name='details["+newCount+"]'>"+detailsArray[index]+"</textarea>\
                                    </div>\
                                <div class='col-md-2'>\
                                    <button type='button' class='btn btn-success btn-sm pull-left' id='"+id+"' onclick=\'RemoveBox(this.id)\' title='Remove Details'>\
                                        <i class='fa fa-minus' aria-hidden='true'></i>\
                                    </button>\
                                </div>\
                            </div>";
                            $(".allDetailsDiv").append(addString);    
                            var detailBoxCount = $("#detailBoxCount").val(newCount);
                        }
                          
                        if(data.image != '')
                        {
                            var addString = '<div class="form-group " >\
                                    <div class="form-group">\
                                            <label for="image" class="col-md-4 control-label">View Image </label>\
                                        <div class="col-md-6">\
                                            <div class="col-md-6">\
                                                    <img src="'+data.image+'" alt="Package" width="100px" class="viewImage">\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="form-group" >\
                                    <div class="form-group">\
                                        <label for="image" class="col-md-4 control-label">Change Image </label>\
                                            <div class="col-md-6">\
                                                <input type="file" name="image" id="image">\
                                            </div>\
                                    </div>\
                                </div>\
                            </div>';

                            $('#imageArea').html('');
                            $('#imageArea').html(addString);
                            $('.newImageArea').hide();
                            
                        }

                    }
                });
            }else{
                $(".title_form_model").text("Create New Package");
            }
            
            $(_model).modal('show');
            return false;
        });


    $('.unitpackage_form').submit(function(event) {
        event.preventDefault();
        var error_msg = "";
        if($(".unitpackage_form #name").val()==""){
            error_msg = "@lang('comman.validation.field_required',['field_name'=>'name'])<br>";
        }
        if($(".unitpackage_form #desc").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'desc'])<br>";
        }
        if($(".unitpackage_form #price").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'price'])<br>";
        }
        if($(".unitpackage_form #currency").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'currency'])<br>";
        }
       /* if($(".unitpackage_form #interval_count").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'Payment Cycle Count'])<br>";
        }*/
        if($(".unitpackage_form #interval").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'Payment Cycle Interval'])<br>";
        }
        var id = $("#package_id").val();
        if(id == 0)
        {
            if($(".unitpackage_form #image").val() != ""){ 
                var file = $('#image').prop('files')[0];
                var fileType = file["type"];
                var ValidImageTypes = ["image/svg", "image/gif", "image/jpg", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    error_msg += "* The image must be a file of type: jpeg, png, jpg, gif, svg.<br>";
                }
            }
            else
            {
                error_msg += "@lang('comman.validation.field_required',['field_name'=>'image'])<br>";
            }
        }
        else
        {
            if($(".unitpackage_form #image").val() != ""){ 
                var file = $('#image').prop('files')[0];
                var fileType = file["type"];
                var ValidImageTypes = ["image/svg", "image/gif", "image/jpg", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    error_msg += "* The image must be a file of type: jpeg, png, jpg, gif, svg.<br>";
                }
            } 
        }
        if($(".unitpackage_form #status").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'status'])<br>";
        }
        var detailBoxCount = $("#detailBoxCount").val();
        console.log(detailBoxCount);
        var array = $(".textarea");
            for (var x = 0; x <= detailBoxCount; x++) { 
                if(array[x].value == ''){
                    error_msg += "@lang('comman.validation.field_required',['field_name'=>'details "+x+"'])<br>";
                }
            }
        if(error_msg!=""){
            $(".form_submit_error").html(error_msg).show().delay(5000).fadeOut();
            return false;
        }
        if($(".unitpackage_form #package_id").val()!="" && $(".unitpackage_form #package_id").val()!=0){
            var id = $(".unitpackage_form #package_id").val();
            var url = "{{url('admin/packages')}}/"+id;
            var method = "POST";
        }
        else
        {
            var url = "{{url('admin/packages')}}";
            var method = "POST";
        }
        $.ajax({
            url: url,
            method: method,
            dataType:'json',
            async:false,
            type:'post',
            processData: false,
            contentType: false,
            data:new FormData($("#unitpackage_form")[0]),
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (data) {
                $(_model).modal('hide');
                toastr.success('Action Success!', data.message);
                datatable.fnDraw(false);
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
    
</script>
@endpush