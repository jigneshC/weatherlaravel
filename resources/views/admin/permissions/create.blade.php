@extends('layouts.admin')

@section('title','Create Permission')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Create Permission
                    </div>
				</div>	
					 <div class="box-content ">
						<a href="{{ url('/admin/permissions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/permissions', 'class' => 'form-horizontal']) !!}

                        @include ('admin.permissions.form')

                        {!! Form::close() !!}
					 </div>
                
			</div>
        </div>
    </div>
@endsection	
