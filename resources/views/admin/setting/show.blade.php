@extends('layouts.admin')
@section('title','View Setting')
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">View Setting</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/setting', $setting->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Setting',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{ $setting->id }}</td>
                            </tr>
                            <tr>
                                <td>Key</td>
                                <td>{{ $setting->key }}</td>
                            </tr>

                            <tr>
                                <td>Value</td>
                                <td>{{ $setting->value }}</td>
                            </tr>
                            
                            <tr>
                                <td>Status</td>
                                <td>{{ $setting->status }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection