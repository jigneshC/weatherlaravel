<div class="modal modal-static fade" id="unitpackage_modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title_form_model"></h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="row rental_option_form_row">
                        {!! Form::open(['url' => '','id'=>'settingform','class' => 'form-horizontal settingform', 'files' => true]) !!}

                        {!! Form::hidden('id',0, ['class' => 'form-control','id'=>'id']) !!}
                        
                        <div class="form-group {{ $errors->has('key') ? 'has-error' : ''}}">
                            {!! Form::label('key', 'Key *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                    <input type="text" name="key" class="filter form-control"  id="key" style="">
                                {!! $errors->first('key', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('value') ? 'has-error' : ''}}">
                            {!! Form::label('value', 'Value *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                    <input type="text" name="value" class="filter form-control"  id="value" style="">
                                {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        

                        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                            {!! Form::label('status', 'Status *',['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::select('status',['' =>'--select--', 'active'=>'Active','inactive'=>'Inactive'] ,null, ['class' => 'form-control']) !!}
                                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <p class="form_submit_error text-error"></p>

                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-primary btn_form_model']) !!}

                        <button type="button" class="btn btn-warning" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('js')


<script>
/***************action model******************/
    var _model = "#unitpackage_modal";
        $(document).on('click', '.setting_form_open', function (e) {
            var id=$(this).attr('data-id');
            $('#settingform')[0].reset();
            $("#id").val(id);
            if(id!=0){
                $(".title_form_model").html("Update Setting");
                var url = "{{url('admin/setting')}}/"+id+"/edit";   
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (result) {
                        var data=result.data;
                        $(".settingform #key").val(data.key);
                        $(".settingform #value").val(data.value);   
                        $('#status [value="'+data.status+'"]').attr('selected', 'true');                     
                    }
                });
            }else{
                $(".title_form_model").html("Create New Setting");
            }
            
            $(_model).modal('show');
            return false;
        });


    $('.settingform').submit(function(event) {

        var error_msg = "";
        if($(".settingform #key").val()==""){
            error_msg = "@lang('comman.validation.field_required',['field_name'=>'key'])<br>";
        }
        /*else
        {   alert(($.trim( $(".settingform #key").val() ).length));
            if((($.trim( $(".settingform #key").val() )).length != ($(".settingform #key").val()).length) != true)
            {
                error_msg += "*key not allow spaces.<br>";
            }
        }*/
        if($(".settingform #value").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'value'])<br>";
        }
        
        if($(".settingform #status").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'status'])<br>";
        }
        if(error_msg!=""){
            $(".form_submit_error").html(error_msg).show().delay(5000).fadeOut();
            return false;
        }
        
        var formData = $("#settingform").serialize();
        
        var url = "{{url('admin/setting')}}";
        var method = "POST";

        if($(".settingform #id").val()!="" && $(".settingform #id").val()!=0){
            var url = "{{url('admin/setting')}}/"+$(".settingform #id").val();
            var method = "PUT";
        }
        $.ajax({
            type: method,
            url: url,
            data: formData,
            success: function (data) {
                $(_model).modal('hide');
                toastr.success('Action Success!', data.message);
                datatable.fnDraw(false);
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
    
</script>
@endpush