<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title','Home') | {{ config('app.name') }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta content='text/html;charset=utf-8' http-equiv='content-type'>
        <meta content='Flat administration template for Twitter Bootstrap. ' name='description'>
        <link href="{!! asset('assets/images/favicon.png') !!}" rel='shortcut icon' type='image/png'>
        @include('themeb.include.cssfiles')
        @yield('headExtra')
        @stack('css')
    </head>

    
    <body class="@yield('body_class','')">
        <div id="wrapper">
            @yield('content')
        </div>
    </body>
    @include('themeb.include.jsfiles')
    @include('themeb.include.page_notification')
    @stack('script-head')
    @stack('js')
</body>
</html>