<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title','Home') | {{ config('app.name') }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta content='text/html;charset=utf-8' http-equiv='content-type'>
        <meta content='Flat administration template for Twitter Bootstrap. ' name='description'>
        <link href="{!! asset('assets/images/favicon.png') !!}" rel='shortcut icon' type='image/png'>
        @include('include.user-backend.cssfiles')
        @yield('headExtra')
        @stack('css')
    </head>

    <body>
        @if (Session::has('session_notification_header1') && Session::get('session_notification_header1')!="")
        <div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            {{Session::get('session_notification_header1')}}
        </div>
        @endif

        @include('include.user-backend.topnav')
        <div class="page-container">
            <div class="page-content">
                @include('include.user-backend.sidebar')
                <div class="content-wrapper">
                    @include('include.user-backend.page_header')
                    <div class="content">
                        @yield('content')
                        @include('include.user-backend.footer')
                    </div>
                </div>
            </div>
        </div>
    </body>
    @include('include.user-backend.jsfiles')
    @include('include.frontend.page_notification')
    @stack('script-head')
    @stack('js')
</body>
</html>