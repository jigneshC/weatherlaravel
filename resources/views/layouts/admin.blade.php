<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. ' name='description'>
    <link href='{!! asset('assets/images/favicon.png') !!}' rel='shortcut icon' type='image/png'>
    @include('include.backend.cssfiles')
    @yield('headExtra')
	@stack('css')
</head>
<body class='contrast-red'>
@include('include.backend.topnav')
<div id='wrapper'>
    <div id='main-nav-bg'></div>
	@include('include.backend.sidebar')
	<section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>
					@include('include.backend.page_notification')
					<div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                @if(View::hasSection('pageHeader'))
                                    @yield('pageHeader')
                                @endif
                            </div>
                        </div>
                    </div>
                    @yield('content')
				</div>
            </div>
			@include('include.backend.footer')
        </div>
    </section>
	
</div>

@include('include.backend.jsfiles')
@stack('script-head')
@stack('js')
</body>
</html>