<!DOCTYPE html>
<html class='no-js' lang='en'>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. ' name='description'>
    <link href='{!! asset('assets/images/favicon.png') !!}' rel='shortcut icon' type='image/png'>
    @include('include.frontend.cssfiles')
    @yield('headExtra')
	@stack('css')
</head>
<body class="@yield('body_class','')">
    

<div id='wrapper'>
    @include('include.frontend.topnav')
    @yield('content')
	@include('include.frontend.footer')
</div>

@include('include.frontend.jsfiles')
@include('include.frontend.page_notification')
@stack('script-head')
@stack('js')
</body>
</html>