<?php

return [

    'mail_subject'=>[
        'user_create_subscription'=>"Subscription Activation !!",
        'user_suspend_subscription'=>"Subscription Suspended !!",
        'user_expired_subscription'=>"Subscription Expired !!",
        'user_invoice_subscription'=>"Subscription Invoice Generated!!",
        'admin_create_subscription'=>"Subscription Activation !!",
        'admin_suspend_subscription'=>"Subscription Suspended !!",
        'admin_expired_subscription'=>"Subscription Expired !!",
        'admin_invoice_subscription'=>"Subscription Invoice Generated!!",
    ],
    'mail_header_lines'=>[
        "user_create_subscription1"=>"You Have Initiate Subscription For :item_name",
        'user_suspend_subscription1'=>"Your Subscription for :item_name Has been Suspended ",
        'user_suspend_subscription2'=>"Please Check your billing cycle to restart Subscription ",
        'user_expired_subscription1'=>"Your Subscription for :item_name Has been expired ",
        'user_expired_subscription2'=>"Your Billing cycle has been stoped and you will be not charge any more for subscripton of :item_name .",
        'user_invoice_subscription1'=>"Invoice for subscription of :item_name has been generated for duration :from to :to ",
        
        "admin_create_subscription1"=>"One new Subscription For :item_name",
        'admin_suspend_subscription1'=>"Subscription for :item_name has been Suspended ",
        'admin_expired_subscription1'=>"Subscription for :item_name has been Expired ",
        'admin_invoice_subscription1'=>"Invoice for subscription of :item_name has been generated for duration :from to :to ",
    ],
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
